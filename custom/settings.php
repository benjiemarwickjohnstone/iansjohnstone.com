<?php

//===================================

// General Settings

$site_title = "Ian S. Johnstone";
$site_address = 'www.iansjohnstone.com'; // no "http://", no trailing "/" !

$copyright_start_year = "2006";
$copyright_owner = "Ian S. Johnstone";

$notification_email = "derry@derrymarwick.com";

// 404 Page Settings
/*TODO impelement custom 404 page properly */
$page_not_found_title = "404 Not Found";
$page_not_found_message = "Sorry, the page ##address## doesn't exist.";

// Database Settings

$database_username = 'bwomj_isj';
$database_password = 'RElhZ5E1';
$database_name = 'bwomj_isj';
$database_server = 'localhost';

// Template Settings

$menuseparator = ' ';

// Recaptcha Settings

$recaptcha_public_key = '6Ld9zwsAAAAAAIg1BsQmZzChj4qltR7-96r60Vb-';
$recaptcha_private_key = '6Ld9zwsAAAAAAOY28MKGXy4gkpTWNcEHs-Er3MCu';

// Contact Settings

$contact_ok_message = 'Message sent successfully';
$contact_send_failed_message = 'Message send failed ... please let us know';
$contact_show_phone = 'false';
$contact_show_address = 'false';

// Comments Settings

$comment_ok_message = 'Your comment was submitted successfully and will be reviewed before appearing on the site.  Thank you!';
$comment_failure_message = 'Comment submit failed ... please let us know';
$comment_show_photos = 'true';

// News Settings

$news_show_photos = '';
$news_show_videos = '';

// Mailing List Settings

$mailing_list_success_message = '';
$mailing_list_error_message = '';

// Photo Settings

$photos_album_not_found_message = 'Album ##album## not found';
$photos_photo_not_found_message = 'Photo ##photo## not found';

//===================================

?>
