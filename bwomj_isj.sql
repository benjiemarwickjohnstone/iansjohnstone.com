-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Nov 28, 2016 at 06:32 PM
-- Server version: 5.5.41-MariaDB
-- PHP Version: 5.6.26

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bwomj_isj`
--

-- --------------------------------------------------------

--
-- Table structure for table `albums`
--

CREATE TABLE `albums` (
  `title` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `id` mediumint(9) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `albums`
--

INSERT INTO `albums` (`title`, `date`, `id`) VALUES
('Spring 2006', '2010-05-10 15:01:35', 3),
('March 2007', '2010-05-10 15:01:45', 4),
('Final Exhibition March 2010', '2010-05-10 15:01:58', 5);

-- --------------------------------------------------------

--
-- Table structure for table `articles`
--

CREATE TABLE `articles` (
  `topic` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `title` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `topicorder` mediumint(9) NOT NULL,
  `articleorder` mediumint(9) NOT NULL,
  `id` mediumint(9) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `articles`
--

INSERT INTO `articles` (`topic`, `title`, `content`, `topicorder`, `articleorder`, `id`) VALUES
('General', 'An Article', 'Lorem ipsum dolor sit amet. £14. −273.15 °C. “Quote.”', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `title` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `author` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `photo` mediumint(9) NOT NULL,
  `id` mediumint(9) NOT NULL,
  `valid` enum('yes','no') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'no'
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `links`
--

CREATE TABLE `links` (
  `topic` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `description` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `topicorder` mediumint(9) NOT NULL,
  `linkorder` mediumint(9) NOT NULL,
  `id` mediumint(9) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `links`
--

INSERT INTO `links` (`topic`, `address`, `description`, `topicorder`, `linkorder`, `id`) VALUES
('General', 'www.lomondweb.net', 'Lomond Web Services', 1, 1, 1),
('General', 'www.bbc.co.uk', 'BBC - £14. −273.15 °C. “Quote.”', 1, 2, 2);

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE `news` (
  `headline` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `text` text COLLATE utf8_unicode_ci NOT NULL,
  `date` datetime NOT NULL,
  `id` mediumint(9) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`headline`, `text`, `date`, `id`) VALUES
('Site set up!', 'This site is now set up and ready for customisation. £14. −273.15 °C. “Quote.”', '2010-05-10 14:51:07', 1);

-- --------------------------------------------------------

--
-- Table structure for table `pages`
--

CREATE TABLE `pages` (
  `title` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `menu` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `address` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL,
  `pageorder` mediumint(9) NOT NULL,
  `showonmenu` enum('true','false') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `location` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `edit_location` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `id` tinytext COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pages`
--

INSERT INTO `pages` (`title`, `menu`, `address`, `content`, `pageorder`, `showonmenu`, `location`, `edit_location`, `id`) VALUES
('Welcome', 'Home', '', 'Welcome to the memorial website for Ian S. Johnstone, Scottish Landscape Artist 1957 - 2009.\r\n##random-photo##\r\n', 1, 'true', '', '', 'home'),
('Obituary', 'Obituary', 'obituary', 'IAN S. JOHNSTONE\r\n\r\nSCOTTISH LANDSCAPE ARTIST\r\n\r\n9th September 1957 - 12th October 2009\r\n\r\n##random-photo##\r\n\r\n     Ian Johnstone was an amazingly talented painter.   But he really didn\'t see himself as an artist.   He just had an absolute passion for landscape and a burning desire to capture and preserve it.   In his own words -\r\n\r\n     "I love the Scottish landscape and all its varied forms ... such a variety of landscape in such a small country ... I love the changing light and weather conditions.   I want to try to fix it ... capture it ... Try and capture some mood or time of day or season ... and hope that it will strike a chord with other people who appreciate landscape."\r\n\r\n     "Because I love it, and it\'s not a job - it\'s a passion.   It\'s an obsession ... it\'s an  urge ... an all-consuming interest.   It\'s not something you can switch on and off, it\'s something I\'ve got to do."\r\n\r\n##random-photo##\r\n\r\n     "It\'s really just a development of my photography.   I can\'t achieve what I want to achieve with photography, so I have to use oil paints.   I\'m not really an artist as such - I\'m not interested in handling paint - it\'s the image that\'s important - or trying to fix the mood of the landscape."\r\n\r\n     " ... when you see something taking shape, when you capture something - that\'s very satisfying, but it\'s not an impressionistic satisfaction - I\'m not trying to express myself.  I want my paintings to be totally detached and have no artistic imprint in terms of brush strokes. ...  Because my paintings are not about painting, they\'re about the actual scene, the actual image, the landscape, they\'re not about me. "\r\n\r\n##random-photo##\r\n\r\n     Ian Stewart Johnstone was born in Edinburgh on September 9th 1957.   He graduated M.A. Hons (Politics) from the University of Edinburgh in 1979, and B.A. Hons (Painting)  from the Edinburgh College of Art in 1984, having been awarded the Catriona White Prize for Drawing.   While still at school he began exhibiting with the Corstorphine Art Group where his father J.H.B. Johnstone was a member, and they exhibited together at The Edinburgh Bookshop, The Hanover Gallery, and the Pitlochry Festival Theatre, where Ian went on to become one of the most popular exhibitors.   Despite success in Art Galleries he continued to support the Dunkeld Cathedral, St Columba\'s Hospice, and Cancer Research UK Exhibitions.   A great many of his paintings are owned privately throughout Britain and abroad.   He was married to the artist Derry Marwick.\r\n\r\n##random-photo##\r\n\r\n     Ian Johnstone was not just talented, but intelligent, principled, and generous.   He was outwardly quiet and reserved, but if you were lucky enough to know him well, he was the most enormous fun to be with.\r\n\r\n##random-photo##\r\n\r\n     " The frustration of it all is I see so many pictures I want to do and I just feel I\'ll never have enough time to do them all. "', 2, 'true', '', '', 'obituary'),
('Gallery', 'Gallery', 'gallery', '', 3, 'true', 'photos.php', 'editphotos.php', 'photos'),
('Contact', 'Contact', 'contact', 'Contact Ian\'s wife, Derry, using the form below:', 4, 'true', 'contact.php', 'editcontact.php', 'contact');

-- --------------------------------------------------------

--
-- Table structure for table `photos`
--

CREATE TABLE `photos` (
  `caption` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `album` mediumint(9) NOT NULL,
  `showasrandom` enum('true','false') COLLATE utf8_unicode_ci NOT NULL DEFAULT 'true',
  `id` mediumint(9) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `photos`
--

INSERT INTO `photos` (`caption`, `album`, `showasrandom`, `id`) VALUES
('Ben More from Glen Dochart', 5, 'true', 9),
('An Teallach', 5, 'true', 8),
('Broughty Ferry from Stannergate', 5, 'true', 7),
('Auchmithie Seascape', 4, 'true', 10),
('Ben Loyal, Sutherland', 3, 'true', 11),
('Ben Starav and Loch Etive', 5, 'true', 12),
('Broughty Castle from the Sands (signed limited edition print)', 5, 'true', 13),
('Broughty Ferry Castle', 5, 'true', 14),
('Buachaille Etive Mor from River Coupall', 5, 'true', 15),
('Castle Stalker and Loch Linnhe', 5, 'true', 16),
('Craigleith from North Berwick', 5, 'true', 17),
('Gruinard Bay', 5, 'true', 18),
('Loch Avon, Cairngorms', 5, 'true', 19),
('Loch Maree and Slioch', 5, 'true', 20),
('Lyne Water, Pentland Hills', 5, 'true', 21),
('Monifieth and Buddon Ness (signed limted edition print)', 5, 'true', 22),
('Mull from Easdale', 5, 'true', 23),
('River Coupall, Glencoe', 5, 'true', 24),
('River Nevis', 5, 'true', 25),
('Rum from Ardnamurchan', 5, 'true', 26),
('Skiddaw from Southerness, Galloway', 5, 'true', 27),
('Tay at Broughty Ferry (signed limited edition print)', 5, 'true', 28),
('The Cuillins from Ord, Skye', 5, 'true', 29),
('The Letterewe Mountains, Wester Ross', 5, 'true', 30),
('Twilight, Esplanade, Broughty Ferry (signed limited edition print)', 5, 'true', 31),
('Ben Nevis from Loch Eil', 3, 'true', 32),
('Bla Bheinn from Loch Slapin, Skye', 3, 'true', 33),
('Creag Leacach, Glenshee', 3, 'true', 34),
('Cuillins of Skye from Arisaig', 3, 'true', 35),
('Eigg and Rum from Arisaig', 3, 'true', 36),
('Glen Affric', 3, 'true', 37),
('Glen Clova', 3, 'true', 38),
('Glen Moriston', 3, 'true', 39),
('Glen Sannox, Arran', 3, 'true', 40),
('Glen Shee', 3, 'true', 41),
('Kiloran Bay, Colonsay', 3, 'true', 42),
('Loch Laidon, Rannoch', 3, 'true', 43),
('Loch Leven', 3, 'true', 44),
('Loch Morloch and the Cairngorms', 3, 'true', 45),
('Loch Scavaigh and the Cuillins, Skye', 3, 'true', 46),
('Morning Light, Perth', 3, 'true', 47),
('Perth and the River Tay', 3, 'true', 48),
('Scurdie Ness from Montrose', 3, 'true', 49),
('Sgurr Nan Gillean from Sligachan, Skye', 3, 'true', 50),
('The Little Gruinard River, Wester Ross', 3, 'true', 51),
('The River Braan, Perthshire', 3, 'true', 52),
('The River Garry near Blair Atholl', 3, 'true', 53),
('The Sound of Sleat from Isleornsay, Skye', 3, 'true', 54),
('Towards Assynt and Coigach from Polbain, Wester Ross', 3, 'true', 55),
('Towards Glen Quoich, Cairngorms', 3, 'true', 56),
('Towards the Angus Coast from Kinshaldy', 3, 'true', 57),
('Glenisla', 4, 'true', 58),
('Glen Shiel, Kintail', 4, 'true', 59),
('Arbroath', 4, 'true', 60),
('Broughty Castle from the Sands', 4, 'true', 61),
('Broughty Ferry Castle', 4, 'true', 62),
('Buachaille Etive Mor', 4, 'true', 63),
('Castle Stalker, Loch Linnhe', 4, 'true', 64),
('Coire Nan Arr, Applecross', 4, 'true', 65),
('Glencoe', 4, 'true', 66),
('Letterewe Mountains, Wester Ross', 4, 'true', 68),
('Loch Voil', 4, 'true', 69),
('Loch Avon, Cairngorms', 4, 'true', 70),
('Loch Clair, Torridon', 4, 'true', 71),
('Loch Duich, Kintail', 4, 'true', 72),
('Loch Hourn', 4, 'true', 73),
('Loch Laggan', 4, 'true', 74),
('Loch Maree and Slioch', 4, 'true', 75),
('Mull from Easdale', 4, 'true', 76),
('Mull from Iona', 4, 'true', 77),
('Rhum from Ardnamurchan', 4, 'true', 78),
('River Ainort, Skye', 4, 'true', 79),
('River Dochart, Killin', 4, 'true', 80),
('River Nevis', 4, 'true', 81),
('Schiehallion from Glen Errochty', 4, 'true', 82),
('Skye from Raasay', 4, 'true', 83),
('The Cuillins from Ord, Skye', 4, 'true', 84);

-- --------------------------------------------------------

--
-- Table structure for table `site`
--

CREATE TABLE `site` (
  `name` tinytext COLLATE utf8_unicode_ci NOT NULL,
  `value` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `site`
--

INSERT INTO `site` (`name`, `value`) VALUES
('sidebar', '+Latest News\n##latest-news##'),
('footer', 'Ian S. Johnstone');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `albums`
--
ALTER TABLE `albums`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `articles`
--
ALTER TABLE `articles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `links`
--
ALTER TABLE `links`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `albums`
--
ALTER TABLE `albums`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `articles`
--
ALTER TABLE `articles`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `links`
--
ALTER TABLE `links`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `photos`
--
ALTER TABLE `photos`
  MODIFY `id` mediumint(9) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=85;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
