<?php

include ('includes.php');
include ('recaptchalib.php');

$content = "";

switch ($_GET['action']) {

case "sendmessage":

  $message = checksubmission();
  if ($message == $contact_ok_message) {
    if (!submitcomment()) {
      $message = $contact_send_failed_message;
    }
    $content = get_text();
    $content .= getform("","","","","");
  } else {
    $content = getform(cleanup($_POST['name']), cleanupemail($_POST['email']), cleanup($_POST['phone']), cleanup($_POST['address']), cleanupemail($_POST['message']));
  }
  break;

default:

  $content = get_text();
  $content .= getform("","","","","");
  break;

}

makepage('contact', $content, $message);

function get_text() {

  $content = "";
  $pagedetailsquery = 'select content from pages where id="contact"';
  if ($pagedetails = mysql_fetch_assoc(mysql_query($pagedetailsquery))) {
    $content = formattextforpage($pagedetails['content']);
  }
  return $content;

}

function getform($name, $email, $phone, $address, $message) {

  global $contact_show_phone, $contact_show_address, $recaptcha_public_key;

  $pagedetailsquery = 'select address from pages where id="contact"';
  $pagedetails = mysql_fetch_assoc(mysql_query($pagedetailsquery));

  $content = '<form action="'.$pagedetails['address'].'?action=sendmessage" method="post" class="medium">';
  $content .= '<div class="formrow"><label for="name">Name</label> <input type="text" id="name" name="name" class="textbox" value="'.cleanup($name).'" /><br /></div>';
  $content .= '<div class="formrow"><label for="email">Email</label> <input type="text" id="email" name="email" class="textbox" value="'.cleanupemail($email).'" /><br /></div>';

  if ($contact_show_phone == 'true') {
    $content .= '<div class="formrow"><label for="phone">Phone</label> <input type="text" id="phone" name="phone" class="textbox" value="'.cleanup($phone).'" /><br /></div>';
  }

  if ($contact_show_address == 'true') {
    $content .= '<div class="formrow"><label for="address">Address</label> <textarea class="short" name="address">'.cleanup($address).'</textarea><br /></div>';
  }

  $content .= '<div class="formrow"><label for="message">Message</label> <textarea class="medium" name="message">'.cleanupemail($message).'</textarea><br /></div>';
  $content .= '<div class="formrow"><label for="recaptcha">&nbsp;</label> '.recaptcha_get_html($recaptcha_public_key).'</div>';
  $content .= '<div class="formrow"><input type="submit" value="Submit" class="button" /></div>';
  $content .= '</form>';
  return $content;

}

function checksubmission() {

  global $contact_ok_message, $recaptcha_private_key, $contact_show_phone, $contact_show_address;

  $resp = recaptcha_check_answer ($recaptcha_private_key, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

  $message = "";
  if (!$resp->is_valid) {
    $message .= 'Sorry, you didn\'t type the two words shown in the picture correctly.  Please try again.  This is to prevent spam.<br />';
  }
  if (cleanup($_POST['name']) != $_POST['name']) {
    $message .= 'Please do not use html, square brackets, email or web addresses in the "Name" field.<br />';
  }
  if (cleanupemail($_POST['email']) != $_POST['email']) {
    $message .= 'Please do not use html or square brackets in the "Email" field.<br />';
  }
  if ($contact_show_phone == 'true' && cleanup($_POST['phone']) != $_POST['phone']) {
    $message .= 'Please do not use html, square brackets, email or web addresses in the "Phone" field.<br />';
  }
  if ($contact_show_address == 'true' && cleanup($_POST['address']) != $_POST['address']) {
    $message .= 'Please do not use html, square brackets, email or web addresses in the "Address" field.<br />';
  }
  if (cleanupemail($_POST['message']) != $_POST['message']) {
    $message .= 'Please do not use html or square brackets in the message body.<br />';
  }
  if (strlen(cleanup($_POST['message'])) < 3) {
    $message .= 'Please write more than three characters in the message body.<br />';
  }
  if ($message == "") {
    $message = $contact_ok_message;
  }

  return $message;

}

function submitcomment() {

global $site_title, $notification_email, $contact_show_phone, $contact_show_address;

  $to = $notification_email;
  $subject = 'Message from '.emailproofstrict($_POST['name'])." on ".$site_title."\r\n";
  $message = 'Name: '.emailproofstrict($_POST['name'])."\r\n";
  $message .= 'Email: '.emailproofstrict($_POST['email'])."\r\n";
  if ($contact_show_phone == 'true') {
    $message .= 'Phone: '.emailproofstrict($_POST['phone'])."\r\n";
  }
  if ($contact_show_address == 'true') {
    $message .= 'Address: '."\r\n".emailproof($_POST['address'])."\r\n\r\n";
  }
  $message .= 'Message: '."\r\n".emailproof($_POST['message']);
  $headers = 'From: contactform@lomondweb.net' . "\r\n" . 'Reply-To: ' . $to . "\r\n" . 'X-Mailer: PHP/' . phpversion();
  if (mail($to, $subject, $message, $headers)) {
    return true;
  }
  return false;

}

?>
