<?php

  include ('includes.php');

  $content = '';

  $newsquery = 'select headline, text, date, id from news order by date desc, id desc';
  $newsitems = mysql_query($newsquery);
  while ($newsitem = mysql_fetch_assoc($newsitems)) {

    $content .= '<div class="newsitem"><a name="'.$newsitem['id'].'"></a>';
    $content .= '<h3>'.$newsitem['headline'].'</h3>';

    if ($newsitem['date'] != '0000-00-00 00:00:00') {
      $content .= '<p>'.gmdate('l jS F Y', strtotime($newsitem['date'])).'</p>';
    }

    $content .= formattextforpage($newsitem['text']);
    $content .= '</div>';

  }

  makepage('news', $content);

?>
