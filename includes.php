<?php

// get site-specific settings
include("custom/settings.php");

// connect to database
$db = mysql_connect($database_server,$database_username,$database_password);
mysql_select_db($database_name);
mysql_set_charset('utf8');

// ###################################

// functions only from this point on

//return the user-friendly address of the page with id $id.
function pageaddress($id) {
  $pagedetailsquery = 'select address from pages where id="'.$id.'"';
  $pagedetails = mysql_fetch_assoc(mysql_query($pagedetailsquery));
  return $pagedetails['address'];
}

// show summary of last 3 news items, with links for more
function latestnews() {
  $content = '';
  $newsquery = 'Select headline, text, date, id from news order by date desc, id desc limit 3';
  $news = mysql_query($newsquery);
  if (mysql_num_rows($news) > 0) {
    while ($newsitem = mysql_fetch_assoc($news)) {
      $newsitemtext = $newsitem['text'];
      $content .= "<h4>".$newsitem['headline']."</h4><p>";
      if ($newsitem['date'] != '0000-00-00 00:00:00') {
        $content .= gmdate('jS F Y', strtotime($newsitem['date'])).'</p><p>';
      }
      if (strpos($newsitemtext, ".")) {
        $content .= substr($newsitemtext, 0, strpos($newsitemtext, "."))."... <a href=\"http://##site-address##/news#".$newsitem['id']."\">more</a></p>";
      } else {
        $content .= $newsitemtext.'</p>';
      }
    }
  }
  $content .= '<p><a href="http://##site-address##/news">all news</a></p>';

  return($content);
}

// show random photograph from those in gallery with showasrandom set to true, with caption and link to full-size photo
function randompicture() {

  global $site_address;

  $content = "";
  $picturequery = 'Select id, caption from photos where showasrandom="true" order by rand()';
  if ($picture = mysql_fetch_assoc(mysql_query($picturequery))) {
    $content .= "<div class=\"photo\"><a href=\"http://".$site_address.'/gallery/photo/'.$picture['id']."\"><img src=\"http://".$site_address.'/images/medium/'.$picture['id'].".jpg\" alt=\"".$picture['caption']."\" /></a><p>".$picture['caption']."</p></div>";
  }
  return $content;

}

// return code to display image $image[1] (from the gallery) inline with caption
function get_image($image) {

  global $site_address;

  $imagequery = 'select caption from photos where id="'.$image[1].'"';
  $imagedetails = mysql_fetch_assoc(mysql_query($imagequery));
  return '<div class="photo"><a href="http://'.$site_address.'/gallery/photo/'.$image[1].'"><img alt="'.$imagedetails['caption'].'" src="http://'.$site_address.'/images/medium/'.$image[1].'.jpg"/></a><p>'.$imagedetails['caption'].'</p></div>';

}

// return code to display image $image[1] (from images/site) inline
function get_picture($image) {

  global $site_address;

  return '<div class="photo"><img alt="" src="http://'.$site_address.'/images/site/'.$image[1].'"/></div>';

}

// create html for whole page using content passed in, 
// getting headings etc from the database entry for this page,
// and echo to browser
function makepage($thispage, $content, $message = "", $isadmin = false) {

  global $copyright_start_year, $copyright_owner, $menuseparator, $site_address, $site_title, $page_not_found_title, $page_not_found_message;

  header("Content-type: text/html; charset=utf-8");

  // if admin page, make sure page is not cached - this ensures contents are up to date
  if ($isadmin) {
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Expires: Mon, 26 Jul 1997 05:00:00 GMT"); // Date in the past
    header("Pragma: no-cache");
  }

  // get page details like title
  $pagedetailsquery = 'Select title, location, edit_location from pages where id="'.$thispage.'"';
  $pagedetailsresult = mysql_query($pagedetailsquery);
  if (mysql_num_rows($pagedetailsresult)) { /*TODO improve clarity of error page code */
    $pagedetails = mysql_fetch_assoc($pagedetailsresult);
  } else {
    $pagedetails = array('title'=>$page_not_found_title, 'location'=>'', 'edit_location'=>'');
    $message = str_replace('##address##', $_SERVER['SERVER_NAME'].$_SERVER['REQUEST_URI'], $page_not_found_message);
  }

  // process message
  if ($message != "") {
    $message = '<div class="message">'.$message.'</div>';
  }

  // make menu
  $menuquery = 'select menu, address, id from pages where showonmenu="true" order by pageorder';
  $menuresult = mysql_query($menuquery);
  $menu = '';
  while ($menuitem = mysql_fetch_assoc($menuresult)) {
    $menu .= '<a href="http://'.$site_address.'/'.$menuitem['address'].'"';
    if ($thispage == $menuitem['id']) {
      $menu .= ' class="selected"';
    }
    $menu .= '><span>'.$menuitem['menu'].'</span></a>'.$menuseparator;
  }

  // make copyright notice
  $copyright = '&copy; '.$copyright_start_year;
  if (date('Y') != $copyright_start_year) {
    $copyright .= '-'.date('Y');
  }
  $copyright .= ' '.$copyright_owner;

  // make control line
  $control = '';
  if (!$isadmin && $pagedetails['location'] == "") {
    $control .= ' <a href="http://##site-address##/admin/editpage.php?page=';
    $control .= $thispage;
    $control .= '">Edit this page</a>';
  } else if (!$isadmin && $pagedetails['edit_location'] != "") {
    $control .= ' <a href="http://##site-address##/admin/';
    $control .= $pagedetails['edit_location'];
      $first = true;    
    foreach ($_GET as $name => $value) {
      if ($first) {$control .= '?';} else {$control .= '&amp;';}
      $first = false;
      $control .= $name.'='.$value;
    }
    $control .= '">Edit this page</a>';
  }

  // make sidebar
  $sidebar = '';
  $sidebarresult = mysql_fetch_assoc(mysql_query('select value from site where name="sidebar"'));
  $sidebar = formattextforpage($sidebarresult['value']);

  // make footer
  $footer = '';
  $footerresult = mysql_fetch_assoc(mysql_query('select value from site where name="footer"'));
  $footer = formattextforpage($footerresult['value']);

  // generate page
  $page = file_get_contents('http://'.$site_address.'/custom/template.html');
  $page = str_replace('##menu##', $menu, $page);
  $page = str_replace('##page-title##', $pagedetails['title'], $page);
  $page = str_replace('##page-content##', $message.$content, $page);
  $page = str_replace('##sidebar-content##', $sidebar, $page);
  $page = str_replace('##footer-content##', $footer, $page);
  $page = str_replace('##copyright##', $copyright, $page);
  $page = str_replace('##control##', $control, $page);
  $page = str_replace("##latest-news##", latestnews(), $page);
  $page = str_replace('##site-address##', $site_address, $page);
  $page = str_replace('##site-title##', $site_title, $page);

  if (!$isadmin) {

    $page = preg_replace('/~([^~|]+)\|([^~|]+)~/', '<a href="$1">$2</a>', $page);
    $page = preg_replace('/~([^~]+)~/', '<a href="$1">$1</a>', $page);
    $page = preg_replace_callback('/##photo-([0-9]+)##/', 'get_image', $page);
    $page = preg_replace_callback('/##picture-([a-zA-Z\-_0-9\.]+)##/', 'get_picture', $page);

    while (($i = strpos($page, "##random-photo##")) !== false) {
      $page = substr_replace($page, randompicture(), $i, strlen("##random-photo##"));
    }
  }
 
  // send completed page to user's browser
  echo $page;

}

// convert text with lomondweb markup to html for sending to browser
function formattextforpage($text) {

  $output = "";

  // get all new lines into \n format
  $text = mb_ereg_replace("/\\r\\n|\\n|\\r/", "\n", $text);

  // process each line in turn
  $lines = explode("\n",$text);
  $insection = "";
  $sectionend = "";
  foreach ($lines as $line) {
    $lineout = "";
    $firstchar = mb_substr($line, 0, 1);
    if ($insection != "html") {
      switch ($firstchar) {
      case "+":
        $lineout .= $sectionend;
        $insection = "";
        $sectionend = "\n";
        if (mb_substr($line, 1, 1) == "+") {
          $lineout .= "<h4>".htmlentities(mb_substr($line, 2), ENT_QUOTES, 'UTF-8')."</h4>";
        } else {
          $lineout .= "<h3>".htmlentities(mb_substr($line, 1), ENT_QUOTES, 'UTF-8')."</h3>";
        }
        break;
      case "{":
        $lineout .= $sectionend;
        $insection = "html";
        $sectionend = "\n";
        break;
      case "#":
        if (mb_substr($line, 1, 1) == "#") {
          $lineout .=  $sectionend;
          $insection = "";
          $sectionend = "\n";
          $lineout .= htmlentities($line, ENT_QUOTES, 'UTF-8');
        } else {
          if ($insection != "ol") {
            $lineout .= $sectionend;
            $lineout .= "<ol>";
          }
          $lineout .= "<li>".htmlentities(mb_substr($line, 1), ENT_QUOTES, 'UTF-8')."</li>";
          $insection = "ol";
          $sectionend = "</ol>\n";
        }

        break;
      case "%":
        $lineout .= $sectionend;
        $insection = "";
        $sectionend = "\n";
        $lineout .= "<p align=\"center\"><img src=\"".trim(htmlentities(mb_substr($line,1), ENT_QUOTES, 'UTF-8'))."\" /></p>";
        break;      
      case "*":
        if ($insection != "ul") {
          $lineout .= $sectionend;
          $lineout .= "<ul>";
        }
        $lineout .= "<li>".htmlentities(mb_substr($line, 1), ENT_QUOTES, 'UTF-8')."</li>";
        $insection = "ul";
        $sectionend = "</ul>\n";
        break;
      case "":
          $lineout .=  $sectionend;
          $insection = "";
          $sectionend = "\n";
        break;
      default:
        if ($insection != "para") {
          $lineout .=  $sectionend;
          $insection = "para";
          $sectionend = "</p>\n";
          $lineout .= "<p>".htmlentities($line, ENT_QUOTES, 'UTF-8');
        } else {
          $lineout .= "<br />\n".htmlentities($line, ENT_QUOTES, 'UTF-8');
        }
        break;
      }
    } else {
      if ($firstchar == "}") {
        $lineout .= $sectionend;
        $insection = "";
        $sectionend = "\n";
      } else {
        $lineout .= $line;
      }
    }
    $output .= $lineout."\n";
  }
  $output .= $sectionend;
  $output = str_replace("[", "<b>", $output);
  $output = str_replace("]", "</b>", $output);
  return $output;
}



function cleanup($text) {

  // remove nasty stuff from text field
  $editedtext = $text;
  $editedtext = strip_tags($editedtext);
  $editedtext = str_replace("[","",$editedtext);
  $editedtext = str_replace("]","",$editedtext);
  $editedtext = str_replace("@","",$editedtext);
  $editedtext = str_replace("www","",$editedtext);
  $editedtext = str_replace("http","",$editedtext);
  $editedtext = str_replace(".html","",$editedtext);
  $editedtext = str_replace(".org","",$editedtext);
  $editedtext = str_replace(".com","",$editedtext);
  $editedtext = str_replace(".net","",$editedtext);
  $editedtext = strip_tags($editedtext);

  return $editedtext;

}


function cleanupemail($text) {

  // remove nasty stuff from text field
  $editedtext = $text;
  $editedtext = strip_tags($editedtext);
  $editedtext = str_replace("[","",$editedtext);
  $editedtext = str_replace("]","",$editedtext);
  $editedtext = strip_tags($editedtext);

  return $editedtext;

}

// function to return parameter $text with Content-Type and newlines stripped
function emailproofstrict($text) {
  $text = preg_replace( "/\n/", " ", $text);
  $text = preg_replace( "/\r/", " ", $text);
  $text = preg_replace( "/Content-Type/", " ", $text);
  return $text;
}

// function to return parameter $text with Content-Type stripped
function emailproof($text) {
  $text = preg_replace( "/Content-Type/", " ", $text);
  return $text;
}


?>
