<?php

  include('includes.php');

  $content = '';

  if (isset($_GET['article'])) {
    $content = getarticle($_GET['article']);
  } else {
    $content = getarticleslist();
  }

  makepage('articles', $content);

  /***************************************/

  function getarticle($id) {

    $content = '';

    $pagedetailsquery = 'select title, address from pages where id="articles"';
    $pagedetails = mysql_fetch_assoc(mysql_query($pagedetailsquery));

    $articlequery = 'select title, content from articles where id="'.$id.'"';
    if ($article = mysql_fetch_assoc(mysql_query($articlequery))) {
      $content = '<div class="article"><h3>'.$article['title'].'</h3>';
      $content .= formattextforpage($article['content']).'</div>';

      $content .= '<div class="links"><p><a href="../'.$pagedetails['address'].'">Back to '.$pagedetails['title'].'</a></p></div>';
    }

    return $content;

  }

  function getarticleslist() {

    $content = '';

    $pagedetailsquery = 'select address from pages where id="articles"';
    $pagedetails = mysql_fetch_assoc(mysql_query($pagedetailsquery));

    $topicquery = 'select distinct topic from articles order by topicorder';
    $topics = mysql_query($topicquery);
    while ($topic = mysql_fetch_assoc($topics)) {

      $content .= '<div class="articlecategory"><h3>'.$topic['topic'].'</h3><ul>';

      $articlequery = 'select title, id from articles where topic="'.$topic['topic'].'" order by articleorder';
      $articles = mysql_query($articlequery);
      while ($article = mysql_fetch_assoc($articles)) {

        $content .= '<li><a href="'.$pagedetails['address'].'/'.$article['id'].'">'.$article['title'].'</a></li>';

      }

      $content .= '</ul></div>';

    }

    return $content;

  }

?>
