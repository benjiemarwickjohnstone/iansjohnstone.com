<?php

  switch ($_GET['step']) {

    case '2':
      echo('<h1>Lomond Web CMS Setup</h1><h2>Step 2 of 5</h2>');
      setup_database();
      echo('<p>Database has been set up</p><p><a href="setup.php?step=3">Next&gt;&gt;</a></p>');
      break;
    case '3':
      echo('<h1>Lomond Web CMS Setup</h1><h2>Step 3 of 5</h2><p>Modify the pages table in the database:</p><ul><li>delete any unwanted pages</li><li>add entries for any plain text pages required</li><li>set the address fields appropriately for all pages</li></ul><p><a href="setup.php?step=4">Next&gt;&gt;</a></p>');
      break;
    case '4':
      echo('<h1>Lomond Web CMS Setup</h1><h2>Step 4 of 5</h2><p>Copy the following into .htaccess:</p>'.htaccess().'<p><a href="setup.php?step=5">Next&gt;&gt;</a></p>');
      break;
    case '5':
      echo('<h1>Lomond Web CMS Setup</h1><h2>Step 5 of 5</h2><ul><li>modify template.html</li><li>modify custom/settings.php</li></ul><p><a href="..">View Site</a></p>');
      break;
    default:
      echo('<h1>Lomond Web CMS Setup</h1><h2>Step 1 of 5</h2><p>Make sure you have done all of the following:</p><ul><li>upload all files</li><li>set up password protection on admin folder</li><li>create database</li><li>add database settings to custom/settings.php</li></ul><p><a href="setup.php?step=2">Next&gt;&gt;</a></p>');
      break;

  }

/***********************************************/

function setup_database() {

// get database settings
include("../custom/settings.php");

// connect to database
$db = mysql_connect($database_server,$database_username,$database_password);
mysql_select_db($database_name);
mysql_set_charset('utf8');

// utf8
$query = "alter database $database_name default character set utf8 collate utf8_unicode_ci";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// pages
$query = "create table pages (title tinytext not null, menu tinytext not null, address tinytext not null, content text not null, pageorder mediumint not null, showonmenu enum('true', 'false') not null default 'true', location tinytext not null, edit_location tinytext not null, id tinytext not null)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// site variables
$query = "create table site (name tinytext not null, value text not null)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// articles
$query = "create table articles (topic tinytext not null, title tinytext not null, content text not null, topicorder mediumint not null, articleorder mediumint not null, id mediumint not null auto_increment primary key)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// links
$query = "create table links (topic tinytext not null, address tinytext not null, description tinytext not null, topicorder mediumint not null, linkorder mediumint not null, id mediumint not null auto_increment primary key)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// comments
$query = "create table comments (title tinytext not null, text text not null, author tinytext not null, date datetime not null, photo mediumint not null, id mediumint not null auto_increment primary key, valid enum('yes', 'no') not null default 'no')";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// news
$query = "create table news (headline tinytext not null, text text not null, date datetime not null, id mediumint not null auto_increment primary key)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// photos
$query = "create table photos (caption tinytext not null, album mediumint not null, showasrandom enum('true', 'false') not null default 'true', id mediumint not null auto_increment primary key)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// albums
$query = "create table albums (title tinytext not null, date datetime not null, id mediumint not null auto_increment primary key)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// pages
$query = "insert into pages (title, menu, address, content, pageorder, showonmenu, location, edit_location, id) values ('Welcome', 'Home', '', 'Welcome to LomondWeb CMS!\n##random-photo##\nLorem ipsum dolor sit amet. £14. −273.15 °C. “Quote.”', 1, 'true', '', '', 'home'), ('About Us', 'About Us', 'about-us', 'Lorem ipsum dolor sit amet.\n##random-photo##\nLorem ipsum dolor sit amet.', 2, 'true', '', '', 'about-us'), ('News', 'News', 'news', '', 3, 'false', 'news.php', 'editnews.php', 'news'), ('Articles', 'Articles', 'articles', '', 4, 'true', 'articles.php', 'editarticles.php', 'articles'), ('Photos', 'Photos', 'photos', '', 5, 'true', 'photos.php', 'editphotos.php', 'photos'), ('Comments', 'Comments', 'comments', '', 6, 'true', 'comments.php', 'editcomments.php', 'comments'), ('Contact', 'Contact', 'contact', '', 7, 'true', 'contact.php', 'editcontact.php', 'contact'), ('Links', 'Links', 'links', '', 8, 'true', 'links.php', 'editlinks.php', 'links')";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// site variables
$query = "insert into site (name, value) values ('sidebar', '+Latest News\n##latest-news##'), ('footer', 'LomondWeb CMS')";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// albums
$query = "insert into albums (title, id) values ('Sample Photos', 1), ('More Photos', 2)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// photos
$query = "insert into photos (caption, album, id) values ('Alhambra', 1, 1), ('Amsterdam', 1, 2), ('Gate', 1, 3), ('Pier', 1, 4), ('Skye', 2, 5), ('Skye', 2, 6)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// news
$query = "insert into news (headline, text, date) values ('Site set up!', 'This site is now set up and ready for customisation. £14. −273.15 °C. “Quote.”', now())";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// links
$query = "insert into links (topic, address, description, topicorder, linkorder) values ('General', 'www.lomondweb.net', 'Lomond Web Services', 1, 1), ('General', 'www.bbc.co.uk', 'BBC - £14. −273.15 °C. “Quote.”', 1, 2)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

// articles
$query = "insert into articles (topic, title, content, topicorder, articleorder) values ('General', 'An Article', 'Lorem ipsum dolor sit amet. £14. −273.15 °C. “Quote.”', 1, 1)";
echo ('<p>'.$query.'</p>');
mysql_query($query);
if (mysql_error() == '') {
  echo ('<p>SUCCESS!</p>');
} else {
  echo ('<p><b>ERROR: </b>'.mysql_error().'</p>');
}

}

/***********************************************/

function htaccess() {

// get database settings
include("../custom/settings.php");

// connect to database
$db = mysql_connect($database_server,$database_username,$database_password);
mysql_select_db($database_name);
mysql_set_charset('utf8');

$output = 'RewriteEngine on'."\n";
$output .= 'RewriteRule ^$ pages.php'."\n";

$pagesquery = 'select address, location, id from pages order by pageorder asc';
$pagesresult = mysql_query($pagesquery);
while ($page = mysql_fetch_assoc($pagesresult)) {

  switch ($page['id']) {

    case 'home':
      break;
    case 'articles':
      $output .= 'RewriteRule ^'.$page['address'].'/([0-9]+)$ articles.php?article=$1'."\n";
      $output .= 'RewriteRule ^'.$page['address'].'$ articles.php'."\n";
      break;
    case 'photos':
      $output .= 'RewriteRule ^'.$page['address'].'/photo/([0-9]+)$ photos.php?photo=$1'."\n";
      $output .= 'RewriteRule ^'.$page['address'].'/album/([0-9]+)$ photos.php?album=$1'."\n";
      $output .= 'RewriteRule ^'.$page['address'].'$ photos.php'."\n";
      break;
    default:
      if ($page['location'] != '') {
        $output .= 'RewriteRule ^'.$page['address'].'$ '.$page['location']."\n";
      } else {
        $output .= 'RewriteRule ^'.$page['address'].'$ pages.php?page='.$page['address']."\n";
      }
      break;
  }

}

return ('<textarea rows="30" cols="100">'.$output.'</textarea>');
}

?>
