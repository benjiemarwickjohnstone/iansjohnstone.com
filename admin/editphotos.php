<?php

include('../includes.php');

$message = "";
$content = "";
$title = "";

switch ($_GET['action']) {

case "processaddalbum":

  $message = addalbum();
  header('Location: http://'.$site_address.'/admin/editphotos.php?action=editalbum&album='.mysql_insert_id());
  $content = getaddalbumform();
  $content .= getalbumlist();
  break;

case "deletealbum":

  $content = getdeletealbumconfirmation();
  $title = "Delete Album";
  break;

case "processdeletealbum":

  $message = reallydeletealbum();
  $content = getaddalbumform();
  $content .= getalbumlist();
  break;

case "renamealbum":

  $content = getrenamealbumform();
  $title = "Rename Album";
  break;

case "processrenamealbum":

  $message = renamealbum();
  $content = getaddalbumform();
  $content .= getalbumlist();
  break;

case "editalbum":

  $content = getaddphotoform();
  $content .= getphotolist();
  $title = "Edit Album";
  break;

case "processaddphoto":

  $message = uploadphoto();
  $content = getaddphotoform();
  $content .= getphotolist();
  $title = "Edit Album";
  break;

case "deletephoto":

  $content = getdeletephotoconfirmation();
  $title = "Delete Photo";
  break;

case "processdeletephoto":

  $message = reallydeletephoto();
  $content = getaddphotoform();
  $content .= getphotolist();
  $title = "Edit Album";
  break;

case "renamephoto":

  $title = "Rename Photo";
  $content = getrenamephotoform();
  break;

case "processrenamephoto":

  $message = renamephoto();
  $content = getaddphotoform();
  $content .= getphotolist();
  $title = "Edit Album";
  break;

case "rotatephoto":

  $message = rotatephoto();
  $content = getaddphotoform();
  $content .= getphotolist();
  $title = "Edit Album";
  break;

default:

  if (isset($_GET['album'])) {
    $content = getaddphotoform();
    $content .= getphotolist();
    $title = "Edit Album";
  } else {
    $content = getaddalbumform();
    $content .= getalbumlist();
  }
  break;

}

makepage("photos", $content, $message, true);

// ###############################

function getalbumlist() {

  $content = '<h3>Existing Albums</h3>';
  $query = 'select title, id from albums order by date desc';
  $result = mysql_query($query);
  while ($row = mysql_fetch_assoc($result)) {
    $content .= "<h4>".$row['title']."</h4><p>";
    $query2 = 'Select caption, id from photos where album="'.$row['id'].'" order by rand() limit 3';
    $result2 = mysql_query($query2);
    while ($row2 = mysql_fetch_assoc($result2)) {
      $content .= "<img src=\"../images/thumbnails/".$row2['id'].".jpg\" />&nbsp;&nbsp;&nbsp;";
    }
    $content .= "...</p>";

    $content .= '<p><a href="editphotos.php?action=editalbum&album='.$row['id'].'">edit</a>';
    $content .= ' <a href="editphotos.php?action=renamealbum&album='.$row['id'].'">rename</a>';
    $content .= ' <a href="editphotos.php?action=deletealbum&album='.$row['id'].'">delete</a></p>';
  }
  return $content;

}

function getaddalbumform() {

  $content = '<h3>Add New Album</h3>';
  $content .= '<form class="narrow" method="POST" action="editphotos.php?action=processaddalbum">';
  $content .= '<div class="formrow"><label for="name">Album Name</label> <input type="text" id="name" name="name" class="textbox" /><br /></div>';
  $content .= '<div class="formrow"><input type="submit" value="Add Album" class="button" /></div>';
  $content .= '</form>';

  return $content;

}

function addalbum() {

  $query = 'insert into albums (title, date) values ("'.$_POST['name'].'", NOW())';
  $result = mysql_query($query);
  if ($result) {
    return "New album added successfully";
  }
  return "Add album failed";

}

function getrenamealbumform() {

  $query = 'select title from albums where id="'.$_GET['album'].'"';
  $result = mysql_query($query);
  if ($row = mysql_fetch_assoc($result)) {
    $content = '<form class="narrow" method="POST" action="editphotos.php?action=processrenamealbum&album='.$_GET['album'].'">';
    $content .= '<div class="formrow"><label for="name">Album Name</label> <input type="text" id="name" name="name" class="textbox" value="'.$row['title'].'"/><br /></div>';
    $content .= '<div class="formrow"><a href="editphotos.php">Cancel</a> <input type="submit" value="Rename Album" class="button" /></div>';
    $content .= '</form>';
  } else {
    $content = 'Album not found';
  }
  return $content;

}

function renamealbum() {

  $query = 'update albums set title="'.$_POST['name'].'" where id="'.$_GET['album'].'"';
  $result = mysql_query($query);
  if ($result) {
   return "Album renamed successfully";
  }
  return "Rename album failed";

}

function getdeletealbumconfirmation() {
  $content = '<p>Do you really want to delete this album?</p>';
  $content .= '<form class="narrow" method="POST" action=editphotos.php?action=processdeletealbum&album='.$_GET['album'].'>';
  $content .= '<input type="hidden" value="'.$_GET['album'].'" name="yesImSureIWantToDeleteAlbum" />';
  $content .= '<div class="formrow"><a href="editphotos.php">Cancel</a> <input type="submit" value="Delete" class="button" /></div>';
  $content .= '</form>';

  return $content;
}

function reallydeletealbum() {

  if ($_GET['album'] == $_POST['yesImSureIWantToDeleteAlbum']) {
    $query = 'delete from albums where id="'.$_POST['yesImSureIWantToDeleteAlbum'].'"';
    if (mysql_query($query)) {
      $query = 'delete from photos where album="'.$_POST['yesImSureIWantToDeleteAlbum'].'"';
      if (mysql_query($query)) {
        return "Album deleted successfully";
      }
    }
  }

  return "Delete album failed";

}

function getaddphotoform() {

  $content = '<h3>Add New Photo</h3>';
  $content .= '<form class="medium" enctype="multipart/form-data" method="POST" action="editphotos.php?action=processaddphoto&album='.$_GET['album'].'">';
  $content .= '<input type="hidden" name="MAX_FILE_SIZE" value="12000000" />';
  $content .= '<div class="formrow"><label for="photo">Photo</label> <input type="file" id="photo" name="photo" class="textbox" /><br /></div>';
  $content .= '<div class="forminforow"><span>photos must be in jpg/jpeg format</span></div>';
  $content .= '<div class="formrow"><label for="name">Caption</label> <input type="text" id="name" name="name" class="textbox" /><br /></div>';
  $content .= '<div class="formrow"><a href="editphotos.php">Cancel</a> <input type="submit" value="Add Photo" class="button" /></div>';
  $content .= '</form>';

  return $content;

}

function getphotolist() {

  $content = '<h3>Existing Photos</h3><div class="itemblock">';
  $query = 'select caption, id from photos where album="'.$_GET['album'].'" order by caption asc';
  $result = mysql_query($query);
  while ($row = mysql_fetch_assoc($result)) {
    $content .= "<p align=\"center\"><img src=\"../images/thumbnails/".$row['id'].".jpg\" /><br />".$row['caption']."</p>";
    $content .= '<p align="center"><a href="editphotos.php?action=renamephoto&photo='.$row['id'].'&album='.$_GET['album'].'">edit caption</a>';
//    $content .= ' <a href="editphotos.php?action=rotatephoto&photo='.$row['id'].'&album='.$_GET['album'].'">rotate</a>';
    $content .= ' <a href="editphotos.php?action=deletephoto&photo='.$row['id'].'&album='.$_GET['album'].'">delete</a></p>';
  }
  $content .= '</div>';
  return $content;

}

function getrenamephotoform() {

  $query = 'select caption, album from photos where id="'.$_GET['photo'].'"';
  $result = mysql_query($query);
  if ($row = mysql_fetch_assoc($result)) {
    $content = '<form class="narrow" method="POST" action="editphotos.php?action=processrenamephoto&album='.$row['album'].'&photo='.$_GET['photo'].'">';
    $content .= '<div class="formrow"><label for="name">Caption</label> <input type="text" id="name" name="name" class="textbox" value="'.$row['caption'].'"/><br /></div>';
    $content .= '<div class="formrow"><a href="editphotos.php?action=editalbum&album='.$_GET['album'].'">Cancel</a> <input type="submit" value="Save" class="button" /></div>';
    $content .= '</form>';
  } else {
    $content = 'Photo not found';
  }
  return $content;

}

function renamephoto() {

  $query = 'update photos set caption="'.$_POST['name'].'" where id="'.$_GET['photo'].'"';
  $result = mysql_query($query);
  if ($result) {
   return "Photo renamed successfully";
  }
  return "Rename photo failed";

}

function getdeletephotoconfirmation() {
  $content = '<p>Do you really want to delete this photo?</p>';
  $content .= '<form class="narrow" method="POST" action=editphotos.php?action=processdeletephoto&album='.$_GET['album'].'&photo='.$_GET['photo'].'>';
  $content .= '<input type="hidden" value="'.$_GET['photo'].'" name="yesImSureIWantToDeletePhoto" />';
  $content .= '<div class="formrow"><a href="editphotos.php?action=editalbum&album='.$_GET['album'].'">Cancel</a> <input type="submit" value="Delete" class="button" /></div>';
  $content .= '</form>';

  return $content;
}

function reallydeletephoto() {

  if ($_GET['photo'] == $_POST['yesImSureIWantToDeletePhoto']) {
    $query = 'delete from photos where id="'.$_POST['yesImSureIWantToDeletePhoto'].'"';
    if (mysql_query($query)) {
      return "Photo deleted successfully";
    }
  }

  return "Delete photo failed";

}

function uploadphoto() {
  $query = 'insert into photos (caption, album, showasrandom) values ("'.$_POST['name'].'", "'.$_GET['album'].'", "false")';
  if (mysql_query($query)) {
    if (resize_image($_FILES['photo']['tmp_name'], '../images/large/'.mysql_insert_id().'.jpg', 550, 550)) {
      if (resize_image($_FILES['photo']['tmp_name'], '../images/medium/'.mysql_insert_id().'.jpg', 250, 250)) {
        if (resize_image($_FILES['photo']['tmp_name'], '../images/thumbnails/'.mysql_insert_id().'.jpg', 100, 100)) {
/*          if (move_uploaded_file($_FILES['photo']['tmp_name'], '../images/originals/'.mysql_insert_id().'.jpg')) {*/
            return "Photo uploaded successfully";
/*          }*/
        }
      }
    }
  }
  return "Add photo failed";
}

function rotatephoto() {

  $query = 'select rotation from photos where id="'.$_GET['photo'].'"';
  $result = mysql_query($query);
  if ($row = mysql_fetch_assoc($result)) {
    $rotation = ($row['rotation']+90)%360;
    $query2 = 'update photos set rotation="'.$rotation.'" where id="'.$_GET['photo'].'"';
    $result2 = mysql_query($query2);
    if (mysql_affected_rows() > 0) {
      if (unlink('../images/large/'.$_GET['photo'].'.jpg') && unlink('../images/medium/'.$_GET['photo'].'.jpg') && unlink('../images/thumbnails/'.$_GET['photo'].'.jpg')) {
        if (resize_image('../images/originals/'.$_GET['photo'].'.jpg', '../images/large/'.$_GET['photo'].'.jpg', 550, 550, $rotation)) {
          if (resize_image('../images/originals/'.$_GET['photo'].'.jpg', '../images/medium/'.$_GET['photo'].'.jpg', 250, 250, $rotation)) {
            if (resize_image('../images/originals/'.$_GET['photo'].'.jpg', '../images/thumbnails/'.$_GET['photo'].'.jpg', 100, 100, $rotation)) {
              return "Photo rotated successfully";
            }
          }
        }
      }
    }
  }
  return "Rotate photo failed";
}

function resize_image($source, $destination, $max_width, $max_height, $rotation=0) {

   $image = imagecreatefromjpeg($source);

   $old_width = imagesx($image);
   $old_height = imagesy($image);
   $ratio = min($max_width/$old_width, $max_height/$old_height);
   if ($ratio < 1) {
      $new_width = $ratio*$old_width;
      $new_height = $ratio*$old_height;
   } else {
      $new_width = $old_width;
      $new_height = $old_height;
   }

   $new_image = imagecreatetruecolor($new_width, $new_height);
   imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $old_width, $old_height);

   if ($rotation == 90 || $rotation == 270) {
     $new_new_image = imagecreatetruecolor($new_height, $new_width);
     $new_new_image = imagerotate($new_image, $rotation, 0);
   } else if ($rotation == 180) {
     $new_new_image = imagecreatetruecolor($new_width, $new_height);
     $new_new_image = imagerotate($new_image, 180, 0);
   } else {
     $new_new_image = $new_image;
   }

   // Output
   $success = imagejpeg($new_new_image, $destination, 90);

   imagedestroy($image);
   imagedestroy($new_image);

  return $success;

}

?>
