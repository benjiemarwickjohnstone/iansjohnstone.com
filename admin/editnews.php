<?php include('../includes.php');

$content = "";
$message = "";

switch ($_GET['action']) {

case "editnewsitem":

  $content = editnewsitem();
  break;

case "processeditnewsitem":

  $message = processeditnewsitem();
  $content = getaddnewsitemform();
  $content .= getexistingnewsitems();
  break;

case "addnewsitem":

  $message = addnewsitem();
  $content = getaddnewsitemform();
  $content .= getexistingnewsitems();
  break;

case "deletenewsitem":

  $content = getdeletenewsitemconfirmation();
  break;

case "processdeletenewsitem":

  $message = reallydeletenewsitem();
  $content = getaddnewsitemform();
  $content .= getexistingnewsitems();
  break;

default:

  $content = getaddnewsitemform();
  $content .= getexistingnewsitems();
  break;

}

makepage("news", $content, $message, true);

// ########################################

function getaddnewsitemform() {
  $content = "<h3>Add News Item</h3>";
  $content .= '<form class="medium" method="POST" action="editnews.php?action=addnewsitem">';
  $content .= '<div class="formrow"><label for="headline">Headline</label> <input type="text" id="headline" name="headline" class="textbox" /><br /></div>';
  $content .= '<div class="formrow"><textarea name="text" id="text" class="medium fullwidth"></textarea><br /></div>';
  $content .= '<div class="formrow"><input type="submit" value="Add News Item" class="button addnews" /></div>';
  $content .= '<div class="formrow"><p>The first sentence, up to the first full stop, will appear in the sidebar.<br />Put text in [square brackets] to make it bold.<br />Use "*" as bullet points.</p></div>';
  $content .= '</form>';
  return $content;
}

function getexistingnewsitems() {

  $content = "<h3>Existing News Items</h3>";
  $query = 'Select headline, text, date, id from news order by date desc, id desc';
  $result = mysql_query($query);
  while ($row = mysql_fetch_assoc($result)) {
    $content .= "<a name=\"".$row['id']."\"></a><h4>".$row['headline']."</h4>";
    if ($row['date'] != "0000-00-00 00:00:00") {
      $content .= '<p>'.gmdate("l jS F Y", strtotime($row['date']))."</p>";
    }
    $content .= formattextforpage($row['text']);
    $content .= '<p><a href="editnews.php?action=editnewsitem&item='.$row['id'].'">edit</a> <a href="editnews.php?action=deletenewsitem&item='.$row['id'].'">delete</a></p>';
  }
  return $content;
}

function addnewsitem() {

  $query = 'insert into news (headline, text, date) values ("'.$_POST['headline'].'", "'.$_POST['text'].'", now())';
  if (mysql_query($query)) {
    return "News item added successfully";
  }
  return "Add news item failed";

}

function getdeletenewsitemconfirmation() {
  $content = '<p>Do you really want to delete this news item?</p>';
  $content .= '<form class="narrow" method="POST" action=editnews.php?action=processdeletenewsitem&item='.$_GET['item'].'>';
  $content .= '<input type="hidden" value="'.$_GET['item'].'" name="yesImSureIWantToDeleteNewsItem" />';
  $content .= '<div class="formrow"><a href="editnews.php">Cancel</a> <input type="submit" value="Delete" class="button" /></div>';
  $content .= '</form>';

  return $content;
}

function reallydeletenewsitem() {

  if ($_GET['item'] == $_POST['yesImSureIWantToDeleteNewsItem']) {
    $query = 'delete from news where id="'.$_POST['yesImSureIWantToDeleteNewsItem'].'"';
    if (mysql_query($query)) {
      return "News item deleted successfully";
    }
  }

  return "Delete news item failed";

}

function editnewsitem() {

  $content = "";
  $query = 'Select headline, text from news where id="'.$_GET['item'].'"';
  $result = mysql_query($query);
  if ($row = mysql_fetch_assoc($result)) {
    $content = '<h3>Edit News Item</h3>';
    $content .= '<form class="medium" method="POST" action="editnews.php?action=processeditnewsitem&item='.$_GET['item'].'">';
    $content .= '<div class="formrow"><label for="headline">Headline</label> <input type="text" id="headline" name="headline" class="textbox" value="'.$row['headline'].'" /><br /></div>';
    $content .= '<div class="formrow"><textarea name="text" id="text" class="medium fullwidth">'.$row['text'].'</textarea><br /></div>';
    $content .= '<div class="formrow"><a href="editnews.php">Cancel</a> <input type="submit" value="Save" class="button" /></div>';
    $content .= '<div class="formrow"><p>The first sentence, up to the first full stop, will appear in the sidebar.<br />Put text in [square brackets] to make it bold.<br />Use "*" as bullet points.</p></div>';
    $content .= '</form>';
  }

  return $content;

}

function processeditnewsitem() {
  $query = 'update news set headline = "'.$_POST['headline'].'" where id = "'.$_GET['item'].'"';
  if (mysql_query($query)) {
    $query = 'update news set text = "'.$_POST['text'].'" where id = "'.$_GET['item'].'"';
    if (mysql_query($query)) {
      return "News item updated successfully";
    }
  }
  return "Editing news item failed";
}

?>
