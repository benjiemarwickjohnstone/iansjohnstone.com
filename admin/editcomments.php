<?php include('../includes.php');

$content = "";
$message = "";
$title = "";

switch ($_GET['action']) {

case "deletecomment";

  $content = getdeletecommentconfirmation();
  break;

case "processdeletecomment";

  $message = deletecomment();
  $content = getunreviewedcomments();
  $content .= getexistingcomments();
  break;

case "acceptcomment";

  $message = approvecomment();
  $content = getunreviewedcomments();
  $content .= getexistingcomments();
  break;

default:

  $content = getunreviewedcomments();
  $content .= getexistingcomments();
  break;

}

makepage("comments", $content, $message, true);

// ########################################

function getunreviewedcomments() {

  $content = "<h3>New Comments</h3>";
  $query = 'select title, text, author, date, id from comments where valid="no" order by date desc';
  $result = mysql_query($query);
  while ($row = mysql_fetch_assoc($result)) {
    $content .= "<h4>".$row['title']."</h4><p>Posted by ".$row['author']." on ".gmdate("l jS F Y", strtotime($row['date']))."</p><p>".nl2br($row['text'])."</p>";
    $content .= '<p><a href="editcomments.php?action=acceptcomment&comment='.$row['id'].'">accept</a> <a href="editcomments.php?action=deletecomment&comment='.$row['id'].'">delete</a></p>';
  }
  return $content;

}


function getexistingcomments() {

  $content = "<h3>Existing Comments</h3>";
  $query = 'select title, text, author, date, id from comments where valid="yes" order by date desc';
  $result = mysql_query($query);
  while ($row = mysql_fetch_assoc($result)) {
    $content .= "<h4>".$row['title']."</h4><p>Posted by ".$row['author']." on ".gmdate("l jS F Y", strtotime($row['date']))."</p><p>".nl2br($row['text'])."</p>";
    $content .= '<p><a href="editcomments.php?action=deletecomment&comment='.$row['id'].'">delete</a></p>';
  }
  return $content;

}

function approvecomment() {

  $query = 'update comments set valid="yes" where id="'.$_GET['comment'].'"';
  $result = mysql_query($query);
  if ($result) {
    return "Comment approved successfully";
  }
  return "Approve comment failed.";

}

function getdeletecommentconfirmation() {
  $content = '<p>Do you really want to delete this comment?</p>';
  $content .= '<form class="narrow" method="POST" action=editcomments.php?action=processdeletecomment&comment='.$_GET['comment'].'>';
  $content .= '<input type="hidden" value="'.$_GET['comment'].'" name="yesImSureIWantToDeleteComment" />';
  $content .= '<div class="formrow"><a href="editcomments.php">Cancel</a> <input type="submit" class="button" value="Delete" /></div>';
  $content .= '</form>';

  return $content;
}

function deletecomment() {

  if ($_GET['comment'] == $_POST['yesImSureIWantToDeleteComment']) {
    $query = 'delete from comments where id="'.$_POST['yesImSureIWantToDeleteComment'].'"';
    if (mysql_query($query)) {
      return "Comment deleted successfully";
    }
  }

  return "Delete comment failed";

}

?>
