<?php include('../includes.php');

$content = "";
$message = "";

switch ($_GET['action']) {

case "editarticle":

  $content = editarticle();
  break;

case "processeditarticle":

  $message = processeditarticle();
  if ($message == "Article updated successfully") { //TODO fix duplicate message from processeditarticle() - should be in settings file somewhere
    header('Location: http://'.$site_address.'/'.pageaddress('articles').'/'.$_GET['article']);
  }
  $content = getexistingarticles();
  break;

/*case "addarticle":

  $message = addarticle();
  $content = getexistingarticles();
  break;*/

case "deletearticle":

  $content = getdeletearticleconfirmation();
  break;

case "processdeletearticle":

  $message = reallydeletearticle();
  $content = getexistingarticles();
  break;

default:

  if (isset($_GET['article'])) {
    $content = editarticle();
  } else {
    $content = getexistingarticles();
  }
  break;

}

makepage("articles", $content, $message, true);

// ########################################
/*
function getaddnewsitemform() {
  $content = "<h3>Add News Item</h3>";
  $content .= '<form class="medium" method="POST" action="editnews.php?action=addnewsitem">';
  $content .= '<div class="formrow"><label for="headline">Headline</label> <input type="text" id="headline" name="headline" class="textbox" /><br /></div>';
  $content .= '<div class="formrow"><textarea name="text" id="text" class="medium fullwidth"></textarea><br /></div>';
  $content .= '<div class="formrow"><input type="submit" value="Add News Item" class="button addnews" /></div>';
  $content .= '<div class="formrow"><p>The first sentence, up to the first full stop, will appear in the sidebar.<br />Put text in [square brackets] to make it bold.<br />Use "*" as bullet points.</p></div>';
  $content .= '</form>';
  return $content;
} */

function getexistingarticles() {

  $content = "<h3>Existing Articles</h3>";

  $pagedetailsquery = 'select address from pages where id="articles"';
  $pagedetails = mysql_fetch_assoc(mysql_query($pagedetailsquery));

  $topicquery = 'select distinct topic from articles order by topicorder';
  $topics = mysql_query($topicquery);
  while ($topic = mysql_fetch_assoc($topics)) {

    $content .= '<div class="articlecategory"><h4>'.$topic['topic'].'</h4><ul>';

    $articlequery = 'select title, id from articles where topic="'.$topic['topic'].'" order by articleorder';
    $articles = mysql_query($articlequery);
    while ($article = mysql_fetch_assoc($articles)) {

      $content .= '<li>'.$article['title'].' - <a href="editarticles.php?action=editarticle&article='.$article['id'].'">edit</a> <a href="editarticles.php?action=deletearticle&article='.$article['id'].'">delete</a></li>';

    }

    $content .= '</ul></div>';

  }

  return $content;

}


/*function addarticle() {

  $query = 'insert into articles (headline, text, date) values ("'.$_POST['headline'].'", "'.$_POST['text'].'", now())';
  if (mysql_query($query)) {
    return "News item added successfully";
  }
  return "Add news item failed";

}*/

function getdeletearticleconfirmation() {
  $content = '<p>Do you really want to delete this article?</p>';
  $content .= '<form class="narrow" method="POST" action=editarticles.php?action=processdeletearticle&article='.$_GET['article'].'>';
  $content .= '<input type="hidden" value="'.$_GET['article'].'" name="yesImSureIWantToDeleteArticle" />';
  $content .= '<div class="formrow"><a href="editarticles.php">Cancel</a> <input type="submit" value="Delete" class="button" /></div>';
  $content .= '</form>';

  return $content;
}

function reallydeletearticle() {

  if ($_GET['article'] == $_POST['yesImSureIWantToDeleteArticle']) {
    $query = 'delete from articles where id="'.$_POST['yesImSureIWantToDeleteArticle'].'"';
    if (mysql_query($query)) {
      return "Article deleted successfully";
    }
  }

  return "Delete article item failed";

}

function editarticle() {

  $content = "";
  $query = 'select topic, title, content from articles where id="'.$_GET['article'].'"';
  $result = mysql_query($query);
  if ($row = mysql_fetch_assoc($result)) {
    $content = '<h3>Edit Article</h3>';
    $content .= '<form class="medium" method="POST" action="editarticles.php?action=processeditarticle&article='.$_GET['article'].'">';
    $content .= '<div class="formrow"><label for="topic">Topic</label> <input type="text" id="topic" name="topic" class="textbox" value="'.$row['topic'].'" /><br /></div>';
    $content .= '<div class="formrow"><label for="title">Title</label> <input type="text" id="title" name="title" class="textbox" value="'.$row['title'].'" /><br /></div>';
    $content .= '<div class="formrow"><textarea name="content" id="content" class="long fullwidth">'.htmlentities($row['content'], ENT_QUOTES, 'UTF-8').'</textarea><br /></div>';
    $content .= '<div class="formrow"><a href="editarticles.php">Cancel</a> <input type="submit" value="Save" class="button" /></div>';
    $content .= '<div class="formrow"><p>Start a line with "+" to make it a heading.<br />Put text in [square brackets] to make it bold.<br />Use "*" as bullet points.<br />Don\'t mess with the bits inside {curly brackets}!</p></div>';
    $content .= '</form>';
  }

  return $content;

}

function processeditarticle() {
  $query = 'update articles set title = "'.$_POST['title'].'" where id = "'.$_GET['article'].'"';
  if (mysql_query($query)) {
    $query = 'update articles set topic = "'.$_POST['topic'].'" where id = "'.$_GET['article'].'"';
    if (mysql_query($query)) {
      $query = 'update articles set content = "'.$_POST['content'].'" where id = "'.$_GET['article'].'"';
      if (mysql_query($query)) {
        return "Article updated successfully";
      }
    }
  }
  return "Editing article failed";
}

?>
