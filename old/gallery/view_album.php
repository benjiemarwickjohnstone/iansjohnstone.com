<?php include("galleryfunctions.php");
include("../common/includes.php");

/*if album exists {

  // create any thumbnails that don't already exist
  for each file in folder {
    if not named *_thumbnail.jpg or *_normal.jpg {
      if thumbnail doesn't exist {
        create it
      }
    }
  } */




      $album = $_GET['album'];
      $album_name = album_name($album);

      $dh  = opendir($album);

      while (false !== ($filename = readdir($dh))) {

         if (($filename != ".") && ($filename != "..") && ($filename != "icon.jpg") && ($album != "uploaded") && ($album != "admin") &&
               (is_file($album.'/'.$filename)) &&
               (substr_count($filename, "_thumbnail") == 0) && (substr_count($filename, "_uploaded") == 0))

            $files[] = $filename;

      }

      if ($files != null) {

         page_top($album_name,1);

         sort($files);

         $count = count($files);
         for ($i = 0; $i < $count; $i++) {

            $filename = $files[$i];

            if (!file_exists($album.'/'.basename($filename,".jpg").'_thumbnail.jpg'))
               make_thumbnail($album, $filename, basename($filename,".jpg")."_thumbnail.jpg");

            $image = imagecreatefromjpeg($album.'/'.basename($filename,".jpg").'_thumbnail.jpg');

            echo '<p align="center"><img src="'.$album.'/'.basename($filename,".jpg")
               .'_thumbnail.jpg" width="'.imagesx($image).'" height="'.imagesy($image)
               .'" alt="'.$filename.'" /><br /><a href="view_photo.php?album='.$album.'&photo='
               .$filename.'">'.photo_name($filename).'</a></p>';

            imagedestroy($image);

         }

      echo '<p align="center"><a href="index.php">Back to Gallery</a></p>';

         page_bottom(true);

      }

      else {
         page_top($gallery_name,1);
         echo "<p>No photos in album</p>";
         page_bottom(true);
      }

?>