<?php

$gallery_web_root = "domains/iansjohnstone.com/public_html/gallery";

function album_name($path) {
   return str_replace('_', ' ', basename($path));
}

function photo_name($path) {
   return str_replace('_', ' ', basename($path, ".jpg"));
}

function resize_image($source, $destination, $max_width, $max_height) {

   $image = imagecreatefromjpeg($source);

   $old_width = imagesx($image);
   $old_height = imagesy($image);
   $ratio = min($max_width/$old_width, $max_height/$old_height);
   if ($ratio < 1) {
      $new_width = $ratio*$old_width;
      $new_height = $ratio*$old_height;
   } else {
      $new_width = $old_width;
      $new_height = $old_height;
   }

   $new_image = imagecreatetruecolor($new_width, $new_height);
   imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width,
   $new_height, $old_width, $old_height);

   // Output
   imagejpeg($new_image, $destination, 100);

   imagedestroy($image);
   imagedestroy($new_image);

}

function rotate_image_left ($album, $photo) {
  // delete the thumbnail so it can be regenerated
  delete_image($album, basename($photo, ".jpg").'_thumbnail.jpg');

  $image = imagecreatefromjpeg('../'.$album.'/'.$photo);

  $old_width = imagesx($image);
  $old_height = imagesy($image);
  $new_width = $old_height;
  $new_height = $old_width;
   
  $new_image = imagecreatetruecolor($new_width, $new_height);
  $new_image = imagerotate($image, 90, 0);

  // Output
  delete_image($album, $photo);
  imagejpeg($new_image, '../'.$album.'/'.$photo, 100);

  echo "rotated $album/$photo left";
}

function rotate_image_right ($album, $photo) {
  // delete the thumbnail so it can be regenerated
  delete_image($album, basename($photo, ".jpg").'_thumbnail.jpg');

  $image = imagecreatefromjpeg('../'.$album.'/'.$photo);

  $old_width = imagesx($image);
  $old_height = imagesy($image);
  $new_width = $old_height;
  $new_height = $old_width;
   
  $new_image = imagecreatetruecolor($new_width, $new_height);
  $new_image = imagerotate($image, 270, 0);

  // Output
  delete_image($album, $photo);
  imagejpeg($new_image, '../'.$album.'/'.$photo, 100);

  echo "rotated $album/$photo right";
}

function rotate_image_180 ($album, $photo) {
  // delete the thumbnail so it can be regenerated
  delete_image($album, basename($photo, ".jpg").'_thumbnail.jpg');

  $image = imagecreatefromjpeg('../'.$album.'/'.$photo);

  $old_width = imagesx($image);
  $old_height = imagesy($image);
  $new_width = $old_height;
  $new_height = $old_width;
   
  $new_image = imagecreatetruecolor($new_width, $new_height);
  $new_image = imagerotate($image, 180, 0);

  // Output
  delete_image($album, $photo);
  imagejpeg($new_image, '../'.$album.'/'.$photo, 100);

  echo "rotated $album/$photo 180 degrees";
}

function make_thumbnail($album, $source_name, $destination_name) {

  resize_image($album."/".$source_name, $album."/".$destination_name, 100, 100);

}

function make_standard($album, $source_name, $destination_name) {

  resize_image($source_name, $destination_name, 600, 600);

}

function create_album($name) {

    global $ftp_server, $ftp_user_name, $ftp_user_pass, $gallery_web_root;

  $name = str_replace(' ', '_', $name);

  if (!file_exists('../'.$name)) {
    // set up ftp connection
    $conn_id = ftp_connect($ftp_server);
    // login with username and password
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
    // try to create the directory
    ftp_chdir($conn_id, $gallery_web_root);
    ftp_mkdir($conn_id, $name);
    ftp_site($conn_id, 'CHMOD 0777 '.$name);
    // close the connection
    ftp_close($conn_id);
  }

  echo "created album $name";

}

function rename_album($old_name, $new_name) {

      $new_name = str_replace(' ', '_', $new_name);

    global $ftp_server, $ftp_user_name, $ftp_user_pass, $gallery_web_root;

    // set up ftp connection
    $conn_id = ftp_connect($ftp_server);
    // login with username and password
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
    // try to rename the file
    ftp_rename ($conn_id, $gallery_web_root.'/'.$old_name, $gallery_web_root.'/'.$new_name);
    // close the connection
    ftp_close($conn_id);


  echo "renamed $album/$old_name to $new_name";

}

function delete_album($name) {

    global $ftp_server, $ftp_user_name, $ftp_user_pass, $gallery_web_root;

    // set up ftp connection
    $conn_id = ftp_connect($ftp_server);
    // login with username and password
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
    // try to rename the file

     $dh  = opendir("../".$name);
      while (false !== ($filename = readdir($dh))) {
         if (($filename != ".") && ($filename != ".."))
            ftp_delete( $conn_id, $gallery_web_root."/".$name."/".$filename);
      }
      closedir($conn_id);

    ftp_rmdir ($conn_id, $gallery_web_root.'/'.$name);
    // close the connection
    ftp_close($conn_id);

  echo "deleted album $name";

}

function move_image($source_album, $destination_album, $photo) {

  echo "moved $photo from $source_album to $destination_album";

}

function rename_image($album, $old_name, $new_name) {
    global $ftp_server, $ftp_user_name, $ftp_user_pass, $gallery_web_root;

  $new_name = str_replace(' ', '_', $new_name);

    // set up ftp connection
    $conn_id = ftp_connect($ftp_server);
    // login with username and password
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
    // try to rename the file
    ftp_rename ($conn_id, $gallery_web_root.'/'.$album.'/'.$old_name, $gallery_web_root.'/'.$album.'/'.$new_name);
    // close the connection
    ftp_close($conn_id);


  echo "renamed $album/$old_name to $new_name";

}

function delete_image($album, $name) {

    global $ftp_server, $ftp_user_name, $ftp_user_pass, $gallery_web_root;

    // set up ftp connection
    $conn_id = ftp_connect($ftp_server);
    // login with username and password
    $login_result = ftp_login($conn_id, $ftp_user_name, $ftp_user_pass);
    // try to rename the file
    ftp_delete ($conn_id, $gallery_web_root.'/'.$album.'/'.$name);
    // close the connection
    ftp_close($conn_id);

  echo "deleted $album/$name";

}

function set_sample_image($album, $photo) {

  copy('../'.$album.'/'.basename($photo,'.jpg').'_thumbnail.jpg', '../'.$album.'/icon.jpg');

  echo "set $photo as sample image of $album";

}

?>