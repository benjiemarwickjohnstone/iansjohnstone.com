<?php include("../../common/includes.php"); page_top("Delete Album"); 
include("../galleryfunctions.php");

if (isset($_POST['areyousure']) && $_POST['areyousure'] == "yesimsure") {

  delete_album($_POST['album']);

$host  = $_SERVER['HTTP_HOST'];
$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$extra = 'index.php';
header("Location: http://$host$uri/$extra");

}



?>
      
<p>Are you sure you want to permanently delete this album and all the photos it contains?</p>

<form method="POST" action="delete_album.php">
Cancel <input type="radio" name="areyousure" value="noimnot" checked="checked" /><br />
Yes, delete it <input type="radio" name="areyousure" value="yesimsure" /><br /><br />
<input type="hidden" name="album" value="<?php echo $_GET['album']; ?>" />
<input type="submit" value="submit" />
</form>

<p>&nbsp;</p>

<?php page_bottom(); ?>