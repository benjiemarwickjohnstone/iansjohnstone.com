<?php

function shrink_picture ($file) {

   $thumbnailpath = dirname($file).'/'.basename($file,"_uploaded.jpg").'.jpg';

   $image = imagecreatefromjpeg($file);

   $old_width = imagesx($image);
   $old_height = imagesy($image);
   $ratio = min(600/$old_width, 600/$old_height);
   $new_width = $ratio*$old_width;
   $new_height = $ratio*$old_height;

   $image_p = imagecreatetruecolor($new_width, $new_height);
   imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width,
   $new_height, $old_width, $old_height);

   // Output
   imagejpeg($image_p, $thumbnailpath, 100);

   imagedestroy($image);
   imagedestroy($image_p);

}





include ("../../common/includes.php");
page_top("Photo Gallery Admin");

if (isset($_FILES['userfile'])) {

  // Set Directory where image is to be stored
  $uploaddir = realpath("..")."/";
/*  if (isset($_POST['album2']) && $_POST['album2'] != "") {
    $uploaddir .= $_POST['album2'];
    if (!file_exists($uploaddir)) {
      umask(0);
      mkdir($uploaddir);
      chown($uploaddir, "bwomj");
    }
    $uploaddir .= "/";
  } else { */
    $uploaddir .= $_POST['album']."/";
/*  } */

  // If title set, use this to generate name, else use original file name
  if ($_POST['title'] == "") {
    $uploadfile = $uploaddir . basename($_FILES['userfile']['name'], ".jpg");
  } else {
    $uploadfile = $uploaddir . str_replace(" ","_",$_POST['title']);
  }

  // if file already exists with this name, append a number to end to make a unique name
  if (file_exists($uploadfile.".jpg")) {
    $num = 1;
    while (file_exists($uploadfile."_".$num.".jpg")) $num++;
    $uploadfile .= "_" . $num;
  }
  $uploadfile .= "_uploaded.jpg";

  // move it to correct place
  if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
     echo "<p>File was successfully uploaded as ".$_POST['album']."/".basename($uploadfile)."<br /><br /></p>\n\n";
  }

  // reduce it to max 600 x 600
  shrink_picture($uploadfile);
  

}

?>

<b>Upload jpg image:</b>
<form enctype="multipart/form-data" action="index.php" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
    Title <input name="title" type="text" /><br />
    Album <select name="album" size="1">

<?php
  $basepath = realpath("..")."/";
  $dh  = opendir($basepath);
  while (false !== ($filename = readdir($dh))) {
    if (($filename != ".") && ($filename != "..") && (is_dir($basepath.$filename)) && ($filename != "admin")) $files[] = $filename;
  }
  sort($files);
  foreach ($files as $file) {
    echo "<option value=\"$file\">$file</option>\n";
  }

?>

    </select><!-- or <input type="text" name="album2" />--><br />
    Image <input name="userfile" type="file" /><br />
    <input type="submit" value="Send File" />
</form>

<?php

  page_bottom();

?>