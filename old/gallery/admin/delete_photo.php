<?php include("../../common/includes.php"); page_top("Delete Photo"); 
include("../galleryfunctions.php");

if (isset($_POST['areyousure']) && $_POST['areyousure'] == "yesimsure") {

  delete_image($_POST['album'], $_POST['photo']);

$host  = $_SERVER['HTTP_HOST'];
$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$extra = 'edit_album.php?album='.$_POST['album'];
header("Location: http://$host$uri/$extra");

}



?>
      
<p>Are you sure you want to permanently delete this photo?</p>

<form method="POST" action="delete_photo.php">
Cancel <input type="radio" name="areyousure" value="noimnot" checked="checked" /><br />
Yes, delete it <input type="radio" name="areyousure" value="yesimsure" /><br /><br />
<input type="hidden" name="album" value="<?php echo $_GET['album']; ?>" />
<input type="hidden" name="photo" value="<?php echo $_GET['photo']; ?>" />
<input type="submit" value="submit" />
</form>

<p>&nbsp;</p>

<?php page_bottom(); ?>