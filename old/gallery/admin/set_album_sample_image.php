<?php include("../../common/includes.php");
include("../galleryfunctions.php"); 
page_top("Set Album Sample Image");

if (isset($_GET['album'])) {

  set_sample_image($_GET['album'],$_GET['photo']);

$host  = $_SERVER['HTTP_HOST'];
$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$extra = 'edit_album.php?album='.$_GET['album'];
header("Location: http://$host$uri/$extra");

}

?>


<p>Photo set as album sample image.</p>

<?php page_bottom(); ?>