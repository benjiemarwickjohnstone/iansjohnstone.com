<?php
include("../../common/includes.php"); 
include ("../galleryfunctions.php"); 


      $album = $_GET['album'];
      $album_name = album_name($album);

page_top("Edit Album - ".$album_name); 




function shrink_picture ($file) {

   $thumbnailpath = dirname($file).'/'.basename($file,"_uploaded.jpg").'.jpg';

   $image = imagecreatefromjpeg($file);

   $old_width = imagesx($image);
   $old_height = imagesy($image);
   $ratio = min(600/$old_width, 600/$old_height);
if ($ratio < 1) {
   $new_width = $ratio*$old_width;
   $new_height = $ratio*$old_height;
} else {
  $new_width = $old_width;
  $new_height = $old_height;
}

   $image_p = imagecreatetruecolor($new_width, $new_height);
   imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width,
   $new_height, $old_width, $old_height);

   // Output
   imagejpeg($image_p, $thumbnailpath, 100);

   imagedestroy($image);
   imagedestroy($image_p);

}



if (isset($_POST['renameto'])) {

  rename_image($album, $old_name, $renameto);

}




for ($i=0; $i<10; $i++) {
$userfile = "upload".$i;

if (isset($_FILES[$userfile])) {

  // Set Directory where image is to be stored
  $uploaddir = realpath("..")."/";
  $uploaddir .= $_POST['album']."/";

  // If title set, use this to generate name, else use original file name
  $uploadfile = $uploaddir . basename($_FILES[$userfile]['name'], ".jpg");

  // if file already exists with this name, append a number to end to make a unique name
  if (file_exists($uploadfile.".jpg")) {
    $num = 1;
    while (file_exists($uploadfile."_".$num.".jpg")) $num++;
    $uploadfile .= "_" . $num;
  }
  $uploadfile .= "_uploaded.jpg";
  $uploadfile = str_replace(" ","_",$uploadfile);

  // move it to correct place
  if (move_uploaded_file($_FILES[$userfile]['tmp_name'], $uploadfile)) {
     echo "<p>File was successfully uploaded as ".$_POST['album']."/".basename($uploadfile)."<br /><br /></p>\n\n";
  }

  // reduce it to max 600 x 600
  shrink_picture($uploadfile);
  
}
}



// ####



echo "<h3>Photos</h3>";

      $dh  = opendir('../'.$album);

      while (false !== ($filename = readdir($dh))) {

         if (($filename != ".") && ($filename != "..") && ($filename != "icon.jpg") && ($album != "uploaded") && ($album != "admin") &&
               (is_file('../'.$album.'/'.$filename)) &&
               (substr_count($filename, "_thumbnail") == 0) && (substr_count($filename, "_uploaded") == 0))

            $files[] = $filename;

      }

      if ($files != null) {


         sort($files);

         $count = count($files);

         echo '<table border="1"><tr><th>Thumbnail</th><th>Caption</th><th>Actions</th></tr>';

         for ($i = 0; $i < $count; $i++) {

            $filename = $files[$i];

            if (!file_exists('../'.$album.'/'.basename($filename,".jpg").'_thumbnail.jpg'))
               make_thumbnail('../'.$album, $filename, basename($filename,".jpg")."_thumbnail.jpg");

            $image = imagecreatefromjpeg('../'.$album.'/'.basename($filename,".jpg").'_thumbnail.jpg');

// ####

echo '<tr><td><img src="../'.$album.'/'.basename($filename,".jpg").'_thumbnail.jpg" width="'.imagesx($image).'" height="'.imagesy($image).'" alt="'.$filename.'" /></td>';
echo '<td><form action="edit_album.php?album='.$album.'" method="POST"><input type="text" name="renameto" value="'.photo_name($filename).'" /> <input type="hidden" name="old_name" value="'.$filename.'" /><input type="submit" value="Rename" /></form></td>';
echo '<td><a href="rotate_photo.php?album='.$album.'&photo='.$filename.'">rotate</a> - <a href="delete_photo.php?album='.$album.'&photo='.$filename.'">delete</a> - <a href="set_album_sample_image.php?album='.$album.'&photo='.$filename.'">set as sample image</a></td></tr>';

// ####

            imagedestroy($image);

         }

      echo '</table>';


      }

      else {

         echo "<p>No photos in album</p>";

      }

// ####

?>

<h3>Upload Photos</h3>
<form enctype="multipart/form-data" action="edit_album.php?album=<?php echo $album; ?>" method="POST">
    <input type="hidden" name="MAX_FILE_SIZE" value="1048576" />
    <input type="hidden" name="album" value="<?php echo $album; ?>" />
<table>
<?php for ($i=0; $i<10; $i+=2) {

echo '<tr><td><input type="file" name="upload';
echo $i;
echo '" /></td><td><input type="file" name="upload';
echo $i+1;
echo '" /></td></tr>';

} ?>
<tr><td colspan="2"><input type="submit" value="upload images" /></td></tr>
</table>
</form>

<p><a href="index.php">Back to Gallery Control Panel</a> - <a href="../index.php">Back to Gallery</a><?php

  if (isset($_GET['album'])) echo " - <a href=\"../view_album.php?album=".$_GET['album']."\">Back to Album</a>";

?></p>

<?php page_bottom(); ?>