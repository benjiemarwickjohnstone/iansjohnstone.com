<?php include("../../common/includes.php"); 
include("../galleryfunctions.php");
page_top("Rotate Photo");

if (isset($_POST['direction'])) {

  if ($_POST['direction'] == "left90") rotate_image_left($_POST['album'],$_POST['photo']);
  if ($_POST['direction'] == "right90") rotate_image_right($_POST['album'],$_POST['photo']);
  if ($_POST['direction'] == "180") rotate_image_180($_POST['album'],$_POST['photo']);

$host  = $_SERVER['HTTP_HOST'];
$uri  = rtrim(dirname($_SERVER['PHP_SELF']), '/\\');
$extra = 'edit_album.php?album='.$_POST['album'];
header("Location: http://$host$uri/$extra");

}

 ?>



<p>In which direction would you like to rotate this photo?</p>

<form action="rotate_photo.php" method="POST">
Left <input type="radio" name="direction" value="left90" /><br />
Right <input type="radio" name="direction" value="right90" /><br />
180 degrees <input type="radio" name="direction" value="180" /><br />
<input type="hidden" name="photo" value="<?php echo $_GET['photo']; ?>" />
<input type="hidden" name="album" value="<?php echo $_GET['album']; ?>" />
<input type="submit" value="Rotate Photo" />
</form>

<p>&nbsp;</p>

<?php page_bottom(); ?>