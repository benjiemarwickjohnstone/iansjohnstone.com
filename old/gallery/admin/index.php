<?php 

  if (isset($_GET['album'])) {
    header("Location: http://www.bwomj.net/photos/admin/edit_album.php?album=".$_GET['album']);
  }

include("../../common/includes.php"); page_top("Gallery Control Panel"); 

include("../galleryfunctions.php");

if (isset($_POST['new_album'])) {

  create_album($_POST['new_album']);

}

if (isset($_POST['album_name'])) {

  rename_album($_POST['old_name'], $_POST['album_name']);

}

?>

<h3>Albums</h3>

<?php

// ########

   $gallery_root = realpath('..')."/";

   $dh  = opendir($gallery_root);

   $files = null;

   while (false !== ($filename = readdir($dh))) {

      if (($filename != ".") && ($filename != "..") && ($filename != "uploaded") && ($filename != "admin") && (is_dir('../'.$filename)))
         $files[] = $filename;

   }

   if ($files != null) {

      sort($files);

      $count = count($files);

      echo '<table border="1"><tr><th>Sample Image</th><th>Album Name</th><th>Actions</th></tr>';

      for ($i = 0; $i < $count; $i++) {

         $filename = $files[$i];

         echo "<tr><td>";

         if (file_exists('../'.$filename.'/icon.jpg')) {
            $image = imagecreatefromjpeg('../'.$filename.'/icon.jpg');
            echo '<img src="../'.$filename.'/icon.jpg" width="'.imagesx($image).'" height="'.imagesy($image)
               .'" alt="'.$filename.'" />';
            imagedestroy($image);
         } else {
            $image = imagecreatefromjpeg('../icon.jpg');
            echo '<img src="../icon.jpg" width="'.imagesx($image).'" height="'.imagesy($image)
               .'" alt="'.$filename.'" />';
            imagedestroy($image);
         }

         echo '<td><form action="index.php" method="POST"><input type="text" name="album_name" value="'.album_name($filename).'" /> <input type="hidden" name="old_name" value="'.$filename.'" /><input type="submit" value="Rename" /></form></td>';
         echo '<td><a href="edit_album.php?album='.$filename.'">edit</a> - <a href="delete_album.php?album='.$filename.'">delete</a></td></tr>';

      }

      echo "</table>";

   }

   else {

      echo "<p>No Albums in Gallery</p>";

   }

   closedir($dh);


// ########

?>

<h3>New Album</h3>
<form action="index.php" method="POST">
Name <input type="text" name="new_album" /> <input type="submit" value="create album" />
</form>
<p><a href="../index.php">Back to Gallery</a></p>

<?php page_bottom(); ?>