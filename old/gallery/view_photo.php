<?php

include("galleryfunctions.php");
include("../common/includes.php");

/* if photo exists or full size image exists {

  // if normal sized photo doesn't exist, create it from full size image
  if photo doesn't exist {
    create photo from full size image
  } */


     $album = $_GET['album'];
      $album_name = album_name($album);
      $photo = $_GET['photo'];
      $photo_name = photo_name($photo);

      $dh  = opendir($album);

      while (false !== ($filename = readdir($dh))) {

         if (($filename != ".") && ($filename != "..") && ($filename != "icon.jpg") && ($album != "uploaded") && ($album != "admin") && (is_file($album.'/'.$filename)) && 
               (substr_count($filename, "_thumbnail") == 0) && (substr_count($filename, "_uploaded") == 0))
            $files[] = $filename;

      }

      sort($files);

      $index = array_search ($photo, $files);

      if(!array_key_exists($index - 1, $files))
         $previous = 'none';
      else
         $previous = $files[$index-1];

      if(!array_key_exists($index + 1, $files))
         $next = 'none';
      else
         $next = $files[$index+1];

      if (file_exists ($album.'/'.$photo) && ($album != "uploaded") && ($album != "admin") ) {

         page_top($album_name,1);

         $image = imagecreatefromjpeg($album.'/'.$photo);

         echo '<p align="center"><img src="'.$album.'/'.$photo
            .'" width="'.imagesx($image).'" height="'.imagesy($image)
            .'" alt="'.$photo.'" /><br />'.$photo_name.'<br />';

         if ($previous != 'none' && (substr_count($photo, "_uploaded") == 0))
            echo '<a href="view_photo.php?album='.$album.'&photo='.$previous.'">&lt;&lt;Previous</a>';

         if ($previous != 'none' && (substr_count($photo, "_uploaded") == 0) && $next != 'none' && (substr_count($photo, "_uploaded") == 0))
            echo ' - ';

         if ($next != 'none' && (substr_count($photo, "_uploaded") == 0))
            echo '<a href="view_photo.php?album='.$album.'&photo='.$next.'">Next&gt;&gt;</a>';

         echo '<br /><a href="view_album.php?album='.$album.'">Back to Album</a> - <a href="index.php">Back to Gallery</a></p>';

         page_bottom(true);

         imagedestroy ($image);

      }

      else {

         page_top($gallery_name,1);
         echo "<p>Photo does not exist</p>";
         page_bottom(true);

      }

?>