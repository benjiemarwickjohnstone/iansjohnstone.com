<?php
include("../common/includes.php");
include("galleryfunctions.php");

   $dh  = opendir('.');

   $files = null;

   while (false !== ($filename = readdir($dh))) {

      if (($filename != ".") && ($filename != "..") && ($filename != "uploaded") && ($filename != "admin") && (is_dir($filename)))
         $files[] = $filename;

   }

   if ($files != null) {

      sort($files);

      $count = count($files);

      page_top('Gallery',1);

      echo "<p>";

      for ($i = 0; $i < $count; $i++) {

         $filename = $files[$i];

         echo '<p align="center">';

         if (file_exists($filename.'/icon.jpg')) {
            $image = imagecreatefromjpeg($filename.'/icon.jpg');
            echo '<img src="'.$filename.'/icon.jpg" width="'.imagesx($image).'" height="'.imagesy($image)
               .'" alt="'.$filename.'" />';
            imagedestroy($image);
         } else {
            $image = imagecreatefromjpeg('icon.jpg');
            echo '<img src="icon.jpg" width="'.imagesx($image).'" height="'.imagesy($image)
               .'" alt="'.$filename.'" />';
            imagedestroy($image);
         }

         echo '<br /><a href="view_album.php?album='
            .$filename.'">'.album_name($filename).'</a></p>'."\n";

      }

      echo "</p>";

      page_bottom(true);

   }

   else {

      page_top($gallery_name,1);
      echo "<p>No albums in gallery</p>";
      page_bottom(true);

   }

   closedir($dh);

?>