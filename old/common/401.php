<?php

include "includes.php";

page_top("401 Unauthorised");

$address = getenv ("REQUEST_URI");

echo "<p>Sorry, you are not authorised to view ".$address.".</p>";
echo "<p>Please try logging in again.</p>";

page_bottom();

?>