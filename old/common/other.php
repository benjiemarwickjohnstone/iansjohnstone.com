<?php

// N.B. Ensure there are no white space characters at the end of the file after php closing tag

  function get_footer_text () {

    return "<p>".date('jS F Y',time()+3600)." - ".date('g:ia',time()+3600-3600)." GMT</p>";

  }

?>