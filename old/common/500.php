<?php

include "includes.php";

page_top("500 Internal Server Error");

$address = getenv ("REQUEST_URI");

echo "<p>Sorry, there has been an internal server error while accessing ".$address.".</p>";
echo "<p>We hope to have this problem fixed shortly.</p>";

page_bottom();

?>