<?php

include "includes.php";

page_top("test_includes.php");
echo "<p>test page for includes.php</p><p>tests page_top($title) and page_bottom() functions and database connection.</p><p>Database is ";
if ($db_working) echo "working"; else echo "<b>NOT</b> working";
echo "</p>";

?>
<p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Sed posuere semper libero. Sed ullamcorper. Quisque laoreet turpis id sem. Mauris aliquet dignissim tellus. Donec blandit suscipit diam. Etiam ante. Aenean urna justo, dictum quis, nonummy nec, lobortis ut, turpis. Donec porttitor dui vitae mauris. Ut vel nibh sed urna mollis feugiat. Maecenas id augue. </p>

<p>Phasellus a orci vel arcu mattis pulvinar. Curabitur fringilla nulla molestie nibh. Donec eu justo in urna semper tempor. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Nunc nunc ipsum, gravida pretium, luctus quis, rutrum ac, tortor. Suspendisse at nisi vel dolor interdum commodo. Duis justo. Morbi turpis libero, laoreet nec, mattis nec, volutpat vitae, ipsum. Vestibulum diam nunc, venenatis eget, imperdiet rhoncus, tincidunt in, sapien. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Quisque ut turpis. </p>

<p>In auctor nibh vitae leo. Nulla malesuada libero sit amet neque ultrices vulputate. Ut vehicula pharetra libero. Nullam felis risus, feugiat eget, volutpat ac, aliquam ac, quam. Sed quis sapien vitae dolor cursus viverra. Fusce libero. Nulla ut elit a velit sollicitudin tempus. Praesent scelerisque velit eget felis. Phasellus a libero vitae ante aliquet feugiat. Maecenas interdum. Etiam arcu dolor, dapibus nec, ultricies quis, lobortis vel, nisl. Nulla augue eros, volutpat non, pellentesque vitae, egestas elementum, tortor. Vivamus at massa eu lectus gravida bibendum. Cras vel lacus non nunc nonummy viverra. Quisque non nisl. Fusce ornare. Maecenas auctor. Morbi bibendum scelerisque pede. Fusce sodales sagittis neque. Integer nisl. </p>

<p>Aenean purus lorem, scelerisque eu, lobortis ut, eleifend non, nisi. Curabitur mollis. Nunc vulputate. Cras urna metus, convallis vitae, posuere eu, pharetra ut, augue. Fusce est. Pellentesque malesuada. Sed id velit. Sed pretium. Donec nonummy elit. Cras blandit nisl at risus. Phasellus pharetra consectetuer ipsum. Vivamus auctor lectus eget velit. Vestibulum aliquam. Praesent sit amet ante vel nisi euismod hendrerit. </p>

<p>Ut blandit. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Sed sit amet sapien. Nam pulvinar tincidunt augue. Curabitur arcu eros, ornare non, eleifend id, volutpat ut, magna. Duis magna metus, suscipit sed, dignissim at, imperdiet quis, felis. Vestibulum dictum, ante quis aliquam viverra, lorem ligula lacinia erat, id faucibus mi nulla a erat. Cras sit amet est a ligula dignissim mattis. Quisque vel dolor. Vestibulum luctus vestibulum justo. </p>

<?
page_bottom();

?>