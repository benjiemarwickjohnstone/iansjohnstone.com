<?php

// N.B. Ensure there are no white space characters at the end of the file after php closing tag

// Settings -------------------------------------

  // Title of site, appears at top of every page
  $site_title = "Ian S. Johnstone";
  // Address to root of site - no "/" at end
  $site_address = "http://www.iansjohnstone.com";

  // Path to site on server - no "/" at end
  $server_path = "/home/bwomj/domains/iansjohnstone.com/public_html/";

  // Main menu for site
    // syntax "Text" => "location"
    // location defined from $site_address
  $menu = array(
    "Home" => "",
    "Gallery" => "/gallery",
    "Links" => "/links"
  );

  // Is a database used for the site?
  $database_used = 1;
  // If so, here are the details:
  $database_name = "bwomj_isj";
  $database_user = "bwomj_isj";
  $database_password = "frogxxx";
  $database_server = "localhost";

  // Style Sheets
  $layout_stylesheets = array(
    "common.css",
    "plain_menu.css",
    "colours.css",
    "custom.css"
  );

  // Is FTP used on the site?
  $ftp_used = 1;
  // If so, here are the details:
  $ftp_server = "localhost";
  $ftp_user_name = "bwomj";
  $ftp_user_pass = "shark";

// End of Settings -----------------------------

?>