<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en"> 

  <head>

    <title>##site_title## - ##page_title##</title>

    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />

    <link rel="shortcut icon" href="##site_address##/favicon.ico" />

    <!-- Basic Text Styles, suitable for all browsers-->
    <link rel="stylesheet" type="text/css" href="##site_address##/common/screen_text_normal.css">

    <!-- Page Layout Styles, @import method prevents older browsers with faulty css support from seeing these styles -->
    <style type="text/css" media="screen">##layout_stylesheets##</style>

  </head>

  <body>

  <div id="container">

    <div id="header" class="clearfix">

      <h1>##site_title##</h1>
        <div id="headerimg" class="clearfix">
          <img src="http://www.iansjohnstone.com/home2.jpg" width="84" height="60" alt="logo2" />
          <img src="http://www.iansjohnstone.com/home3.jpg" width="90" height="60" alt="logo3" /> 
          <img src="http://www.iansjohnstone.com/home1.jpg" width="91" height="60" alt="logo1" />
        </div>
    </div>

    <div id="menu" class="clearfix">

      ##menu##

    </div>

    <div id="content" class="clearfix">

      <h2>##page_title##</h2>

      ##content##

    </div>

    <div id="footer" class="clearfix">

      ##footer##

    </div>

    <div id="control" class="clearfix">

      ##control##

    </div>

  </div>

  </body>

</html>