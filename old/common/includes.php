<?php

// N.B. Ensure there are no white space characters at the end of the file after php closing tag

  include ("settings.php");
  include ("other.php");

  function page_top($page_title = "", $selected_tab = "-1") {

    global $server_path, $site_title, $site_address, $menu, $layout_stylesheets, $db_working, $database_used;

    $top=file_get_contents($server_path."/common/template.php");

    $top = substr($top, 0, strpos($top, "##content##"));

    // Retrieve Menu text from array
    $menutext = '<a href="http://www.iansjohnstone.com">Home</a><a href="http://www.iansjohnstone.com/gallery">Gallery</a><a href="http://www.iansjohnstone.com/links">Links</a>&nbsp;
';

    // retrieve layout stylesheets from array
    $stylesheetstext = "";
    foreach ($layout_stylesheets as $sheet) {
      $stylesheetstext .= "@import url(".$site_address."/common/".$sheet."); ";
    }

    $top = str_replace("##site_title##", $site_title, $top);
    $top = str_replace("##page_title##", $page_title, $top);
    $top = str_replace("##site_address##", $site_address, $top);
    $top = str_replace("##menu##", $menutext."&nbsp;", $top);
    $top = str_replace("##layout_stylesheets##", $stylesheetstext, $top);
    $top = str_replace("##more_stylesheets##", "", $top);

    echo $top;

  }

  function page_bottom($editable = false) {

    global $server_path, $site_address;

    $bottom=file_get_contents($server_path."/common/template.php");

    $bottom = substr($bottom, strpos($bottom, "##content##")+11);

    if ($editable) {
      $thispage = substr($_SERVER['SCRIPT_NAME'],0,strrpos($_SERVER['SCRIPT_NAME'],"/"));
      if ($thispage == "") $thispage .= "/pages";
      $adminlink = "<p><a href=\"".$site_address.$thispage."/admin";
      $thispage = substr($_SERVER['SCRIPT_NAME'],0,strrpos($_SERVER['SCRIPT_NAME'],"/"));
      if ($thispage == "") $adminlink .= "?id=-1";
      elseif (isset($_GET['id'])) $adminlink .= "?id=".$_GET['id'];
      if (!isset($_GET['id']) && isset($_GET['album'])) $adminlink .= "?album=".$_GET['album'];
      if (!isset($_GET['id']) && isset($_GET['album']) && isset($_GET['photo'])) $adminlink .= "&photo=".$_GET['photo'];
      $adminlink .= "\" >Edit this Page</a></p>";
      $bottom = str_replace("##control##", $adminlink, $bottom);
    } else {
      $bottom = str_replace("##control##", "<p>&nbsp;</p>", $bottom);
    }

    $bottom = str_replace("##footer##",get_footer_text(),$bottom);

    echo $bottom;

    exit();

  }

?>