<?php

include "includes.php";

page_top("404 File Not Found");

$address = getenv ("REQUEST_URI");

echo "<p>Sorry, file ".$address." does not exist.</p>";

page_bottom();

?>