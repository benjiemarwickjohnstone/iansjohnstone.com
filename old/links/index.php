<?php

// --- Settings ---------------

   $links_title = "Links";

// -----------------------------



   include_once "../common/includes.php";

   page_top($links_title,2);
?>

<h3>Galleries and Exhibitions</h3>
<p>
Eduardo Allessandro Studios, Broughty Ferry -
<a href="http://www.eastudios.com">www.eastudios.com</a>
<br/>
Pitlochry Festival Theatre -
<a href="http://www.pitlochry.org.uk">www.pitlochry.org.uk</a>
<br/>
Dunkeld Cathedral -
<a href="http://www.dunkeldcathedral.org.uk">www.dunkeldcathedral.org.uk</a>
<br/>
Derry Marwick: Artist -
<a href="http://www.derrymarwick.com">www.derrymarwick.com</a>
<br/>
</p>
<h3>Other</h3>
<p>
Scot-Trek: Guided Walks and Holidays around Scotland -
<a href="http://www.scot-trek.co.uk">www.scot-trek.co.uk</a>
<br/>
</p>

<?php

   page_bottom(true);

?>