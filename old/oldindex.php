<html>
<head>
<title>Paintings by Ian S. Johnstone</title>
</head>
<body bgcolor="#333366">
<h1 style="font-family:verdana; color:#eeeaea;">Ian S. Johnstone</h1>

<h3 style="font-family:verdana; color:#eeeaea;">Email:</h1>

<p style="font-family:verdana; color:#eeeaea;">ian (at) iansjohnstone (dot) com</p>

<h3 style="font-family:verdana; color:#eeeaea;">Sample Paintings:</h1>

<?php

if ($handle = opendir('pictures')) {

   while (false !== ($file = readdir($handle))) {

      if ($file != "." && $file != "..")    {

         echo "<center><img src=\"pictures/".$file."\" /><br />".str_replace(".jpg","",$file)."<br /><br /></center>";
         }
      }
   closedir($handle); 

   }

?>

</body>
</html>