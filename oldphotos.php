<?php

  include ('includes.php');

  $content = '';
  $message = '';

  if (isset($_GET['photo'])) {
    $content = getphoto($_GET['photo']);
    if ($content == '') {
      $message = str_replace('##photo##', $_GET['photo'], $photos_photo_not_found_message);
      $content = getalbumslist();
    }
  } else if (isset($_GET['album'])) {
    $content = getalbum($_GET['album']);
    if ($content == '') {
      $message = str_replace('##album##', $_GET['album'], $photos_album_not_found_message);
      $content = getalbumslist();
    }
  } else {
    $content = getalbumslist();
  }

  makepage('photos', $content, $message);

/*************************************/

  function getalbumslist() {

    $content = '';

    $albumsquery = 'select title, id from albums order by date desc';
    $albums = mysql_query($albumsquery);
    while ($album = mysql_fetch_assoc($albums)) {

      $content .= '<div class="albumsummary"><h3>'.$album['title'].'</h3>';

      $thumbnailsquery = 'select id, caption from photos where album="'.$album['id'].'" order by rand() limit 5';
      $thumbnails = mysql_query($thumbnailsquery);
      while ($thumbnail = mysql_fetch_assoc($thumbnails)) {
        $content .= '<a href="'.pageaddress('photos').'/photo/'.$thumbnail['id'].'"><img src="images/thumbnails/'.$thumbnail['id'].'.jpg" alt="'.$thumbnail['caption'].'" /></a>';
      }

      $content .= '<p><a href="'.pageaddress('photos').'/album/'.$album['id'].'">view whole album</a></p></div>';

    }

    return $content;

  }

  function getalbum($albumid) {

    $content = '';

    $albumquery = 'select title from albums where id="'.$albumid.'"';
    if ($album = mysql_fetch_assoc(mysql_query($albumquery))) {

      $content .= '<h3>'.$album['title'].'</h3>';

      $photosquery = 'select id, caption from photos where album="'.$albumid.'" order by caption asc, id asc';
      $photos = mysql_query($photosquery);
      while ($photo = mysql_fetch_assoc($photos)) {
        $content .= '<div class="photo"><a href="../photo/'.$photo['id'].'"><img src="../../images/medium/'.$photo['id'].'.jpg" alt="'.$photo['caption'].'" /></a><p>'.$photo['caption'].'</p></div>';
      }

      $content .= '<p><a href="../../'.pageaddress('photos').'">back to '.pagename('photos').'</a></p>';

    }

    return $content;

  }

  function getphoto($photoid) {

    $content = '';

    $photoquery = 'select caption, album from photos where id="'.$photoid.'"';
    if ($photo = mysql_fetch_assoc(mysql_query($photoquery))) {

      $content .= '<h3>'.$photo['caption'].'</h3>';
      $content .= '<div class="photo"><img src="../../images/large/'.$photoid.'.jpg" alt="'.$photo['caption'].'" /><p>'.$photo['caption'].'</p></div>';
      $content .= '<p><a href="../../'.pageaddress('photos').'">back to '.pagename('photos').'</a> - <a href="../album/'.$photo['album'].'">back to Album</a></p>';

    }

    return $content;

  }

?>
