<?php

  include ('includes.php');

  $content = '';
  $message = '';

  if (isset($_GET['photo'])) {
    $content = getphoto($_GET['photo']);
    if ($content == '') {
      $message = str_replace('##photo##', $_GET['photo'], $photos_photo_not_found_message);
      $content = getalbumslist();
    }
  } else if (isset($_GET['album'])) {
    $content = getalbum($_GET['album']);
    if ($content == '') {
      $message = str_replace('##album##', $_GET['album'], $photos_album_not_found_message);
      $content = getalbumslist();
    }
  } else {
    $content = getalbumslist();
  }

  makepage('photos', $content, $message);

/*************************************/

  function getalbumslist() {

    $content = '';

    $albumsquery = 'select title, id from albums order by date desc';
    $albums = mysql_query($albumsquery);
    while ($album = mysql_fetch_assoc($albums)) {

      $content .= '<div class="albumsummary"><h3>'.$album['title'].'</h3>';

      $thumbnailsquery = 'select id, caption from photos where album="'.$album['id'].'" order by caption asc, id asc limit 5';
      $thumbnails = mysql_query($thumbnailsquery);
      while ($thumbnail = mysql_fetch_assoc($thumbnails)) {
        $content .= '<a href="gallery/photo/'.$thumbnail['id'].'"><img src="images/thumbnails/'.$thumbnail['id'].'.jpg" alt="'.$thumbnail['caption'].'" /></a>';
      }

      $content .= '<p><a href="gallery/album/'.$album['id'].'">view whole album</a></p></div>';

    }

    return $content;

  }

  function getalbum($albumid) {

    $content = '';

    $albumquery = 'select title from albums where id="'.$albumid.'"';
    if ($album = mysql_fetch_assoc(mysql_query($albumquery))) {

      $content .= '<h3>'.$album['title'].'</h3>';

      $photosquery = 'select id, caption from photos where album="'.$albumid.'" order by caption asc, id asc';
      $photos = mysql_query($photosquery);
      while ($photo = mysql_fetch_assoc($photos)) {
        $content .= '<div class="photo"><a href="../photo/'.$photo['id'].'"><img src="../../images/large/'.$photo['id'].'.jpg" alt="'.$photo['caption'].'" /></a><p>'.$photo['caption'].'</p></div>';
      }

      $content .= '<p><a href="../../gallery">back to Gallery</a></p>';

    }

    return $content;

  }

  function getphoto($photoid) {

    $content = '';

    $photoquery = 'select caption, album from photos where id="'.$photoid.'"';
    if ($photo = mysql_fetch_assoc(mysql_query($photoquery))) {

      $content .= '<h3>'.$photo['caption'].'</h3>';
      $content .= '<div class="photo"><img src="../../images/large/'.$photoid.'.jpg" alt="'.$photo['caption'].'" /><p>'.$photo['caption'].'</p></div>';

      $prevnextpics = array('', '', '', '', '');      
      $thumbnailsquery = 'select id, caption from photos where album="'.$photo['album'].'" order by caption asc, id asc';
      $thumbnails = mysql_query($thumbnailsquery);
      $found = 'false';
      while (($thumbnail = mysql_fetch_assoc($thumbnails)) && ($prevnextpics[0] == '' || $found != 'true')) {
        $prevnextpics[0] = $prevnextpics[1];
        $prevnextpics[1] = $prevnextpics[2];
        $prevnextpics[2] = $prevnextpics[3];
        $prevnextpics[3] = $prevnextpics[4];
        $prevnextpics[4] = $thumbnail['id'];
        if ($prevnextpics[2] == $photoid) {
          $found = 'true';
        }
      }
      $content .= '<div class="albumsummary">';
      $albumquery = 'select title from albums where id="'.$photo['album'].'"';
      if ($album = mysql_fetch_assoc(mysql_query($albumquery))) {
        $content .= '<h3>'.$album['title'].'</h3>';
      }
      for ($pic = 0; $pic < 5; $pic++) {
        if ($prevnextpics[$pic] != '') {
          $content .= '<a href="../../gallery/photo/'.$prevnextpics[$pic].'"><img ';
          if ($prevnextpics[$pic] == $photoid) {
            $content .= 'class="selected" ';
          }
          $content .= 'src="../../images/thumbnails/'.$prevnextpics[$pic].'.jpg" alt="" /></a>';
        }
      }
      $content .= '<p><a href="../../gallery/album/'.$photo['album'].'">view whole album</a> - <a href="../../gallery">back to Gallery</a></p></div>';

    }

    return $content;

  }

?>
