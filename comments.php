<?php

include ('includes.php');
include ('recaptchalib.php');

$content = '';
$message = '';

switch ($_GET['action']) {

case "addcomment":

  $message = checksubmission();
  if ($message == $comment_ok_message) {
    if (!submitcomment()) {
      $message = $comment_failure_message;
    }
    $content = getcomments();
    $content .= getform("","","");
  } else {
    $content .= getform(strip_tags($_POST['title']), strip_tags($_POST['text']), strip_tags($_POST['author']));
  }
  break;

default:

  $content = getcomments();
  $content .= getform("","","");
  break;

}

makepage('comments', $content, $message);

function getcomments() {

  $content = '<p><a href="#addcomment">Add a comment</a></p>';
  // perform query
  $query = 'Select title, text, date, author from comments where valid="yes" order by date desc';
  $result = mysql_query($query);
  // get next result until none left
  while ($row = mysql_fetch_assoc($result)) {
    $content .= "<h4>".$row['title']."</h4><p>Posted by ".$row['author']." on ".gmdate("l jS F Y", strtotime($row['date']))."<br />".nl2br($row['text'])."</p>";
  }
  return $content;

}

function getform($title, $text, $author) {

  global $comment_show_photos, $recaptcha_public_key;

  $pagedetailsquery = 'select address from pages where id="comments"';
  $pagedetails = mysql_fetch_assoc(mysql_query($pagedetailsquery));

  $content .= '<a name="addcomment"></a>';
  $content .= '<h3>Add a Comment</h3>';
  $content .= '<form class="medium" action="'.$pagedetails['address'].'?action=addcomment" method="POST">';
  $content .= '<div class="formrow"><label for="author">Your Name</label> <input type="text" id="author" name="author" class="textbox" value="'.cleanup($author).'" /><br /></div>';
  $content .= '<div class="formrow"><label for="title">Comment Title</label> <input type="text" id="title" name="title" class="textbox" value="'.cleanup($title).'" /><br /></div>';
  $content .= '<div class="formrow"><label for="message">Message</label> <textarea rows="6" cols="40" name="text" id="text" class="medium">'.cleanup($text).'</textarea><br /></div>';
  if ($comment_show_photos == 'true') {

  }
  $content .= '<div class="formrow"><label for="recaptcha">&nbsp;</label> '.recaptcha_get_html($recaptcha_public_key).'</div>';
  $content .= '<div class="formrow"><input type="submit" value="Submit" class="button" /></div>';
  $content .= '</form>';
  return $content;

}

function checksubmission() {

  global $comment_ok_message, $recaptcha_private_key, $comment_show_photos;

  $resp = recaptcha_check_answer ($recaptcha_private_key, $_SERVER["REMOTE_ADDR"], $_POST["recaptcha_challenge_field"], $_POST["recaptcha_response_field"]);

  $message = "";
  if (!$resp->is_valid) {
    $message .= 'Sorry, you didn\'t type the two words shown in the picture correctly.  Please try again.  This is to prevent spam.<br />';
  }
  if (cleanup($_POST['author']) != $_POST['author']) {
    $message .= 'Please do not use html, square brackets, email or web addresses in the "Your Name" field.<br />';
  }
  if (cleanup($_POST['title']) != $_POST['title']) {
    $message .= 'Please do not use html, square brackets, email or web addresses in the "Comment Title" field.<br />';
  }
  if (cleanup($_POST['text']) != $_POST['text']) {
    $message .= 'Please do not use html, square brackets, email or web addresses in the comment text.<br />';
  }
  if (strlen(strip_tags($_POST['text'])) < 3) {
    $message .= 'Please write more than three characters in the comment body.<br />';
  }
  if ($message == "") {
    $message = $comment_ok_message;
  }

  return $message;

}

function submitcomment() {

  global $site_title, $notification_email;

  $query = 'insert into comments (title, text, author, date, valid) values ("'.cleanup($_POST['title']).'", "'.cleanup($_POST['text']).'", "'.cleanup($_POST['author']).'", NOW(), "no")';
  $result = mysql_query($query);
  if ($result && mysql_affected_rows() > 0) {
    $to = $notification_email;
    $subject = 'New Comment on '.$site_title;
    $message = 'A new comment has been submitted at '.$site_title;
    $headers = 'From: commentsform@lomondweb.net' . "\r\n" . 'Reply-To: ' . $to . "\r\n" . 'X-Mailer: PHP/' . phpversion();
    if (mail($to, $subject, $message, $headers)) {
      return true;
    }
  }
  return false;

}

?>

